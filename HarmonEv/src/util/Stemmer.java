package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.harmonev.conceptnet.ConceptNetAPI;
import com.harmonev.conceptnet.Language;
import com.harmonev.conceptnet.models.Uri;
import com.harmonev.wordnet.WordNetUtil;

public class Stemmer {
	public enum StemmerType {WordNet, ConceptNet};
	
	public static ArrayList<String> getWordsOfInterest(String text, StemmerType type, boolean byConcept) {
		Set<String> words = new HashSet<String>(Arrays.asList(text.split(" ")));
		ArrayList<String> result = new ArrayList<String>();
		
		for (String word : words) {

			if (type == StemmerType.WordNet) {
				List<String> cleanedWordList = WordNetUtil.getStemmer().findStems(word, null);
				
				for (String cleanedWord : WordNetUtil.getFilteredSet(cleanedWordList)) {
					result.add(cleanedWord);
				}
				
			} else if (type == StemmerType.ConceptNet) {
				Uri uri = ConceptNetAPI.getUri(Language.EN, word);
				if (byConcept) {
					result.add(uri.getConcept());
				} else {
					List<String> cleanedWordList = Arrays.asList(uri.getConcept().split("_"));
					
					for (String cleanedWord : WordNetUtil.getFilteredSet(cleanedWordList)) {
						result.add(cleanedWord);
					}
				}
			}
		}
		
		if (result.isEmpty()) {
			System.out.println("No stem found for: " + text);
		}
		
		return result;
	}
}
