package util;

public class Matrix {
	private float[][] baseMatrix;

	public Matrix(float[][] matrix) {
		this.baseMatrix = matrix;
	}
	
	public float[] getMarkovChain(int iteration) {
		return this.multiplyArray(pow(iteration));
	}
	
	private float[][] pow(int iteration) {
		int length = baseMatrix.length;
		float[][] resultMatrix = baseMatrix;
		int iter = 0;
		
		while (iter < iteration) {
			float[][] tempMatrix = new float[length][length];
			for (int i = 0; i < length; i++) {
				for (int j = 0; j < length; j++) {
					float temp = 0;
					
					for (int k = 0; k < length; k++) {
						temp += resultMatrix[i][k] * baseMatrix[k][j];
					}

					tempMatrix[i][j] = temp;
				}
			}

			resultMatrix = tempMatrix;
			iter++;
		}
		
		return resultMatrix;
	}
	
	private float[] multiplyArray(float[][] matrix) {
		int length = matrix.length;
		float[] resultArray = new float[length];
		float[] tempArray = new float[length];
		
		for (int i = 0; i < length; i++) {
			tempArray[i] = 1.0f / length;
		}
		
		for (int i = 0; i < length; i++) {
			float temp = 0;
			
			for (int j = 0; j < length; j++) {
				temp += matrix[i][j] * tempArray[j];
//				System.out.println(matrix[i][j] + " " + tempArray[j]);
			}

			resultArray[i] = temp;
		}
		
		return resultArray;
	}
	
	public String toString(float[][] matrix) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				sb.append(matrix[i][j] + " ");
			}
			
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public void printResult(int iteration) {
		float[] result = getMarkovChain(iteration);
		for (float value : result) {
			System.out.print(value + " ");
		}
	}
	
	public static void main(String[] args) {
		float[][] matrix = {{0.5f,0.3f,0.2f},{0.4f,0.5f,0.1f},{0.4f,0.4f,0f}};
		Matrix m = new Matrix(matrix);
		System.out.println(m.toString(m.pow(5)));
		System.out.println();
		System.out.println(m.toString(matrix));
		System.out.println();
		float[] f = m.multiplyArray(m.pow(10));
		System.out.println();
		System.out.println(f[0] + " " + f[1] + " " + f[2] + " ");
	}
}
