package util;

import java.io.BufferedReader;
import java.io.FileReader;

public class TextFileUtils {
	
	public static GlueList<String> load(String filepath) {
		try (BufferedReader br = new BufferedReader(new FileReader(filepath))) {
		    String line;
		    GlueList<String> lines = new GlueList<String>();
		    while ((line = br.readLine()) != null) {
		    	lines.add(line);
		    }
		    
		    return lines;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
