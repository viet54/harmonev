package com.harmonev.conceptnet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.harmonev.conceptnet.DC.AssociationDC;
import com.harmonev.conceptnet.DC.IDC;
import com.harmonev.conceptnet.DC.LookupDC;
import com.harmonev.conceptnet.DC.UriDC;
import com.harmonev.conceptnet.models.Association;
import com.harmonev.conceptnet.models.Lookup;
import com.harmonev.conceptnet.models.Uri;
import com.harmonev.conceptnet.uri.ConceptURI;

public class ConceptNetAPI {
	private static final String C_NET_LOCAL_URL = "http://127.0.0.1:8084";
	private static final String C_NET_BASE_URL = "http://conceptnet5.media.mit.edu";
	private static final boolean USE_LOCAL_API = true;
	public static final String VERSION = "5.4";
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private static StringBuilder sb = new StringBuilder();
	private static IDC[] multiDC;
	
	private enum RequestType {Lookup, Uri, Search, Association}
	
	private static IDC get(RequestType type, String uri) throws ClientProtocolException, IOException {
		sb.setLength(0);
		sb.append(USE_LOCAL_API ? C_NET_LOCAL_URL : C_NET_BASE_URL);
		sb.append(uri);
		
		CloseableHttpClient client = HttpClientBuilder.create().build();
		CloseableHttpResponse response = client.execute(new HttpGet(sb.toString()));
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		IDC dc = null;
		switch (type) {
		case Lookup:
			dc = gson.fromJson(rd, LookupDC.class);
			break;
		case Uri:
			dc = gson.fromJson(rd, UriDC.class);
			break;
		case Search:
			break;
		case Association:
			dc = gson.fromJson(rd, AssociationDC.class);
			break;
		}
		
		client.close();
		response.close();
		
		return dc;
	}
	
	private static IDC[] multiGet(RequestType type, String[] uris) throws Exception {
		multiDC = new IDC[uris.length];
		
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(100);
        CloseableHttpClient client = HttpClients.custom().setConnectionManager(cm).build();

		try {
            // create a thread for each URI
            GetThread[] threads = new GetThread[uris.length];
            for (int i = 0; i < threads.length; i++) {
            	sb.setLength(0);
            	sb.append(USE_LOCAL_API ? C_NET_LOCAL_URL : C_NET_BASE_URL);
            	sb.append(uris[i]);
            	
                HttpGet httpget = new HttpGet(sb.toString());
                threads[i] = new GetThread(type, client, httpget, i);
            }

            // start the threads
            for (int j = 0; j < threads.length; j++) {
                threads[j].start();
            }

            // join the threads
            for (int j = 0; j < threads.length; j++) {
                threads[j].join();
            }

        } finally {
        	client.close();
        }
		
		return multiDC;
	}
	
	public static Lookup getLookup(ConceptURI conceptUri, int limit) {
		sb.setLength(0);
		sb.append("/data");
		sb.append("/" + VERSION);
		sb.append(conceptUri.getURI());
		sb.append("?limit=" + limit);
		
		try {
			LookupDC dc = (LookupDC) get(RequestType.Lookup, sb.toString());
			Lookup lookup = new Lookup(dc);
			return lookup;
		} catch (Exception e) {
		}
		
		return null;
	}
	
	public static Lookup getLookup(ConceptURI conceptUri) {
		return getLookup(conceptUri, 50);
	}
	
	public static Uri getUri(Language lang, String text) {
		sb.setLength(0);
		sb.append("/data");
		sb.append("/" + VERSION);
		sb.append("/uri?");
		sb.append("language=" + lang);
		sb.append("&text=" + text.replace(" ", "_"));
		
		try {
			UriDC dc = (UriDC) get(RequestType.Uri, sb.toString());
			Uri uri = new Uri(dc);
			return uri;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Lookup getSearch(Relations rel, String attribute, String value, int limit) {
		sb.setLength(0);
		sb.append("/data");
		sb.append("/" + ConceptNetAPI.VERSION);
		sb.append("/search");
		sb.append("?rel=" + rel.getValue());
		sb.append("&" + attribute + "=" +  value);
		sb.append("&limit=" + limit);
		
		try {
			LookupDC dc = (LookupDC) get(RequestType.Lookup, sb.toString());
			Lookup lookup = new Lookup(dc);
			return lookup;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static Association getAssociation(ConceptURI conceptUri) {
		return getAssociation(conceptUri, 50);
	}
	
	public static Lookup getSearch(String startConcept, String endConcept, int limit) {
		sb.setLength(0);
		sb.append("/data");
		sb.append("/" + ConceptNetAPI.VERSION);
		sb.append("/search");
		sb.append("?start=" + new ConceptURI(startConcept).getURI());
		sb.append("&end=" + new ConceptURI(endConcept).getURI());
		sb.append("&limit=" + limit);
		
		try {
			LookupDC dc = (LookupDC) get(RequestType.Lookup, sb.toString());
			Lookup lookup = new Lookup(dc);
			return lookup;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Lookup getSearch(String firstConcept, String secondConcept) {
		return getSearch(firstConcept, secondConcept, 50);
	}
	
	public static Association getAssociation(ConceptURI conceptUri, int limit) {
		try {
			AssociationDC dc = (AssociationDC) get(RequestType.Association, buildAssociationUri(conceptUri, limit));
			Association association = new Association(dc);
			return association;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static float getAssociationBetweenConcepts(ConceptURI firstConceptUri, ConceptURI secondConceptUri) {
		try {
			AssociationDC dc = (AssociationDC) get(RequestType.Association, buildAssociationUri(firstConceptUri, secondConceptUri));
			Association association = new Association(dc);
			return association.getFirstSimilarFloat();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return 0f;
	}
	
	public static float[] multiGetAssociation(ConceptURI[] firstConceptUris, ConceptURI[] secondConceptUris) {
		String[] uris = new String[firstConceptUris.length];
		float[] values = new float[firstConceptUris.length];
		for (int i = 0; i < firstConceptUris.length; i++) {
			uris[i] = buildAssociationUri(firstConceptUris[i], firstConceptUris[i]);
		}
		
		try {
			IDC[] dcs = multiGet(RequestType.Association, uris);
			for (int i = 0; i < dcs.length; i++) {
				Association association = new Association((AssociationDC) dcs[i]);
				values[i] = association.getFirstSimilarFloat();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return values;
	}
	
	private static String buildAssociationUri(ConceptURI ConceptUri, int limit) {
		sb.setLength(0);
		sb.append("/data");
		sb.append("/" + ConceptNetAPI.VERSION);
		sb.append("/assoc");
		sb.append(ConceptUri.getURI());
		sb.append("?filter=/c/en");
		sb.append("&limit=" + limit);
		return sb.toString();
	}
	
	private static String buildAssociationUri(ConceptURI firstConceptUri, ConceptURI secondConceptUri) {
		sb.setLength(0);
		sb.append("/data");
		sb.append("/" + ConceptNetAPI.VERSION);
		sb.append("/assoc");
		sb.append(firstConceptUri.getURI());
		sb.append("?filter=");
		sb.append(secondConceptUri.getURI() + "/.");
		
		return sb.toString();
	}
	
	public static Gson getGson() {
		return gson;
	}
	
    /**
     * A thread that performs a GET.
     */
    static class GetThread extends Thread {

    	private final RequestType requestType;
        private final CloseableHttpClient httpClient;
        private final HttpContext context;
        private final HttpGet httpget;
        private final int id;

        public GetThread(RequestType requestType, CloseableHttpClient httpClient, HttpGet httpget, int id) {
        	this.requestType = requestType;
            this.httpClient = httpClient;
            this.context = new BasicHttpContext();
            this.httpget = httpget;
            this.id = id;
        }

        /**
         * Executes the GetMethod and prints some status information.
         */
        @Override
        public void run() {
            try {
                System.out.println(id + " - about to get something from " + httpget.getURI());
                CloseableHttpResponse response = httpClient.execute(httpget, context);
                try {
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                    	BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
                    	
                		switch (requestType) {
                		case Lookup:
                			multiDC[id] = gson.fromJson(rd, LookupDC.class);
                			break;
                		case Uri:
                			multiDC[id] = gson.fromJson(rd, UriDC.class);
                			break;
                		case Search:
                			break;
                		case Association:
                			multiDC[id] = gson.fromJson(rd, AssociationDC.class);
                			break;
                		}
                    }
                } finally {
                    response.close();
                }
            } catch (Exception e) {
                System.out.println(id + " - error: " + e);
            }
        }

    }
}
