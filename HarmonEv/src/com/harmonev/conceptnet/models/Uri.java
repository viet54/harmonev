package com.harmonev.conceptnet.models;

import com.harmonev.conceptnet.DC.UriDC;

public class Uri extends Model {
	private String uri;

	public Uri(UriDC dc) {
		super(dc);
		
		this.uri = dc.getUri();
	}

	public String getConcept() {
		for (int i = uri.length() - 1; i >= 0; i--) {
			if (uri.charAt(i) == '/') {
				return uri.substring(i + 1, uri.length());
			}
		}

		return null;
	}
}
