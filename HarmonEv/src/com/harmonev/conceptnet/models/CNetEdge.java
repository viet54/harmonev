package com.harmonev.conceptnet.models;

import com.harmonev.conceptnet.DC.CNetEdgeDC;
import com.harmonev.conceptnet.uri.ConceptURI;
import com.harmonev.conceptnet.uri.UriHierarchy;

public class CNetEdge extends Model {
	private String rel;
	private UriHierarchy end;
	private UriHierarchy start;
	private String surfaceEnd;
	private String surfaceStart;
	private float weight;
	
	public CNetEdge(CNetEdgeDC dc) {
		super(dc);
		
		rel = dc.getRel();
		surfaceStart = dc.getSurfaceStart();
		surfaceEnd = dc.getSurfaceEnd();
		end = dc.getEnd() == null ? null : ConceptURI.parseURI(dc.getEnd());
		start = dc.getStart() == null ? null : ConceptURI.parseURI(dc.getStart());
		weight = dc.getWeight();
	}
	
	public String getRel() {
		return rel.replace("/r/", "");
	}

	public String getSurfaceStart() {
		return surfaceStart;
	}

	public String getSurfaceEnd() {
		return surfaceEnd;
	}
	
	public UriHierarchy getEnd() {
		return end;
	}

	public UriHierarchy getStart() {
		return start;
	}

	public float getWeight() {
		return weight;
	}
}
