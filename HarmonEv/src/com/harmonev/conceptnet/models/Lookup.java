package com.harmonev.conceptnet.models;

import com.harmonev.conceptnet.DC.LookupDC;

public class Lookup extends Model {
	private CNetEdge[] edges;
	
	public Lookup (LookupDC dc) {
		super(dc);
		
		this.edges = new CNetEdge[dc.getEdges().length];
		for (int i = 0; i < dc.getEdges().length; i++) {
			edges[i] = new CNetEdge(dc.getEdges()[i]);
		}
	}
	
	public CNetEdge[] getEdges() {
		return edges;
	}
	
}
