package com.harmonev.conceptnet.models;

import com.harmonev.conceptnet.ConceptNetAPI;
import com.harmonev.conceptnet.DC.IDC;

public abstract class Model {
	private String json;
	private IDC dc;
	
	public Model (IDC dc) {
		this.dc = dc;
		this.json = ConceptNetAPI.getGson().toJson(dc);
	}
	
	public IDC getDC() {
		return dc;
	}

	public String getJson() {
		return json;
	}
}
