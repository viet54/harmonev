package com.harmonev.conceptnet.models;

import com.harmonev.conceptnet.DC.AssociationDC;

public class Association extends Model {
	
	public Association (AssociationDC dc) {
		super(dc);
	}
	
	public String[][] getSimilarValues() {
		return ((AssociationDC) getDC()).getSimilar();
	}

	public float getFirstSimilarFloat() {
		if (getSimilarValues().length > 0 && getSimilarValues()[0].length > 0) {
			return Float.parseFloat(getSimilarValues()[0][1]);
		}
		return 0f;
	}
}
