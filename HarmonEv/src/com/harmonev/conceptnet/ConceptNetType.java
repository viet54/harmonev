package com.harmonev.conceptnet;

import com.google.common.collect.HashBiMap;

public enum ConceptNetType {
	A,
	C,
	D,
	E,
	L,
	R,
	S,
	AND,
	OR;
	
	private static final HashBiMap<ConceptNetType, String> MAP = HashBiMap.create(9);
	
	static {
		MAP.put(A, "/a");
		MAP.put(C, "/c");
		MAP.put(D, "/d");
		MAP.put(E, "/e");
		MAP.put(L, "/l");
		MAP.put(R, "/r");
		MAP.put(S, "/s");
		MAP.put(AND, "/and");
		MAP.put(OR, "/or");
	}
	
	public String getValue() {
		return getValue(this);
	}
	
	public static String getValue(ConceptNetType type) {
		return MAP.get(type);
	}
	
	public static ConceptNetType getType(String val) {
		return MAP.inverse().get(val);
	}
}
