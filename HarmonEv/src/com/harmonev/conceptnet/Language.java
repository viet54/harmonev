package com.harmonev.conceptnet;

import com.google.common.collect.HashBiMap;

public enum Language {
	EN;
	
	private static final HashBiMap<Language, String> MAP = HashBiMap.create(1);
	
	static {
		MAP.put(EN, "en");
	}
	
	public String getValue() {
		return getValue(this);
	}
	
	public static String getValue(Language lang) {
		return MAP.get(lang);
	}
	
	public static Language getLanguage(String val) {
		return MAP.inverse().get(val);
	}
}
