package com.harmonev.conceptnet.DC;

public class CNetEdgeDC implements IDC {
	private String id;
    private String surfaceEnd;
    private String surfaceStart;
    private float weight;
    private String source_uri;
    private String start;
    private String dataset;
    private String[] features;
    private String context;
    private String surfaceText;
    private String rel;
    private String license;
    private String uri;
    private String[] sources;
    private String end;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getSurfaceEnd ()
    {
        return surfaceEnd;
    }

    public void setSurfaceEnd (String surfaceEnd)
    {
        this.surfaceEnd = surfaceEnd;
    }

    public String getSurfaceStart ()
    {
        return surfaceStart;
    }

    public void setSurfaceStart (String surfaceStart)
    {
        this.surfaceStart = surfaceStart;
    }

    public float getWeight ()
    {
        return weight;
    }

    public void setWeight (float weight)
    {
        this.weight = weight;
    }

    public String getSource_uri ()
    {
        return source_uri;
    }

    public void setSource_uri (String source_uri)
    {
        this.source_uri = source_uri;
    }

    public String getStart ()
    {
        return start;
    }

    public void setStart (String start)
    {
        this.start = start;
    }

    public String getDataset ()
    {
        return dataset;
    }

    public void setDataset (String dataset)
    {
        this.dataset = dataset;
    }

    public String[] getFeatures ()
    {
        return features;
    }

    public void setFeatures (String[] features)
    {
        this.features = features;
    }

    public String getContext ()
    {
        return context;
    }

    public void setContext (String context)
    {
        this.context = context;
    }

    public String getSurfaceText ()
    {
        return surfaceText;
    }

    public void setSurfaceText (String surfaceText)
    {
        this.surfaceText = surfaceText;
    }

    public String getRel ()
    {
        return rel;
    }

    public void setRel (String rel)
    {
        this.rel = rel;
    }

    public String getLicense ()
    {
        return license;
    }

    public void setLicense (String license)
    {
        this.license = license;
    }

    public String getUri ()
    {
        return uri;
    }

    public void setUri (String uri)
    {
        this.uri = uri;
    }

    public String[] getSources ()
    {
        return sources;
    }

    public void setSources (String[] sources)
    {
        this.sources = sources;
    }

    public String getEnd ()
    {
        return end;
    }

    public void setEnd (String end)
    {
        this.end = end;
    }

    @Override
    public String toString()
    {
        return "EdgeDC [id = "+id+", surfaceEnd = "+surfaceEnd+", surfaceStart = "+surfaceStart+", weight = "+weight+", source_uri = "+source_uri+", start = "+start+", dataset = "+dataset+", features = "+features+", context = "+context+", surfaceText = "+surfaceText+", rel = "+rel+", license = "+license+", uri = "+uri+", sources = "+sources+", end = "+end+"]";
    }
}
