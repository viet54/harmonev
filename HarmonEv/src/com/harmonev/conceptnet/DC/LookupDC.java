package com.harmonev.conceptnet.DC;

public class LookupDC implements IDC {
	private CNetEdgeDC[] edges;
    private String numFound;

    public CNetEdgeDC[] getEdges ()
    {
        return edges;
    }

    public void setEdges (CNetEdgeDC[] edges)
    {
        this.edges = edges;
    }

    public String getNumFound ()
    {
        return numFound;
    }

    public void setNumFound (String numFound)
    {
        this.numFound = numFound;
    }
    
    @Override
    public String toString()
    {
        return "Data [edges = "+edges+", numFound = "+numFound+"]";
    }
}
