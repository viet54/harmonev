package com.harmonev.conceptnet.DC;


public class UriDC implements IDC {
	private String uri;

    public String getUri ()
    {
        return uri;
    }

    public void setUri (String uri)
    {
        this.uri = uri;
    }

	@Override
    public String toString()
    {
        return "UriDC [uri = "+uri+"]";
    }
}
