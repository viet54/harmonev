package com.harmonev.conceptnet.DC;

public class AssociationDC implements IDC {
	private String[][] terms;
    private String[][] similar;
    
    public String[][] getTerms ()
    {
        return terms;
    }

    public void setTerms (String[][] terms)
    {
        this.terms = terms;
    }

    public String[][] getSimilar ()
    {
        return similar;
    }

    public void setSimilar (String[][] similar)
    {
        this.similar = similar;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [terms = "+terms+", similar = "+similar+"]";
    }
}
