package com.harmonev.conceptnet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSinkDGS;
import org.graphstream.stream.file.FileSourceDGS;

import util.GlueList;
import util.TextFileUtils;

public class Assertions {
	private String rawFilePath = "E:/ConceptNet/data/AssertionsSplittedFiles/assertions";
	private String englishOnlyFilePath = "E:/ConceptNet/data/assertions_english_only.csv";
	private String relationStartEnd = "E:/ConceptNet/data/relation_start_end.csv";
	
	private String relationStartEndFinal = "E:/ConceptNet/data/relation_start_end_final.csv";
	private String relationStartEndCleanFinal = "E:/ConceptNet/data/relation_start_end_clean_final.csv";
	private String assertionGraphDGSFinal = "E:/ConceptNet/data/assertion_graph__DGS_final.dgs";
	
	private static String styleSheet =
			"graph {" +
            "	text-size: 20;" +
            "}" +
			"node {" +
            "	fill-color: black;" +
            "	text-size: 16;" +
            "}" +
            "node.root {" +
            "	fill-color: red;" +
            "}" +
            "node.intersection {" +
            "	fill-color: blue;" +
            "}" +
            "edge {" +
            "	text-size: 16;" +
            "}" +
            "edge.common {" +
            "	fill-color: purple;" +
            "}";
	
	/*
	Antonym
	AtLocation
	CapableOf
	Causes
	CausesDesire
	CreatedBy
	DefinedAs
	DerivedFrom
	Desires
	DistinctFrom
	Entails
	EtymologicallyRelatedTo
	FormOf
	HasA
	HasContext
	HasFirstSubevent
	HasLastSubevent
	HasPrerequisite
	HasProperty
	HasSubevent
	InstanceOf
	IsA
	LocatedNear
	MadeOf
	MannerOf
	MotivatedByGoal
	NotCapableOf
	NotDesires
	NotHasProperty
	PartOf
	ReceivesAction
	RelatedTo
	SimilarTo
	SymbolOf
	Synonym
	UsedFor
	dbpedia/capital
	dbpedia/field
	dbpedia/genre
	dbpedia/genus
	dbpedia/influencedBy
	dbpedia/knownFor
	dbpedia/language
	dbpedia/leader
	dbpedia/occupation
	dbpedia/product
	*/
	
	public void filterEnglishOnly() {
		StringBuilder sb = new StringBuilder();
		String[] index = new String[]{"01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
				"11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24",
				"25", "26", "27", "28", "29"};
		
		int totalLines = 0;
		for (String i : index) {
			GlueList<String> lines = TextFileUtils.load(rawFilePath + "_0000" + i + ".csv");
			
			int p = 0;
			for (String s : lines) {
				String[] values = s.split("\t");
				String startNodeLanguage = values[2].split("/")[2];
				String endNodeLanguage = values[3].split("/")[2];
				
				if (startNodeLanguage.equals("en") && endNodeLanguage.equals("en")) {
					sb.append(s + "\n");
					
					totalLines++;
					p++;
					if (p == 5000) {
						System.out.println(s);
						p = 0;
					}
				}
			}
			
			writeFile(sb.toString(), englishOnlyFilePath);
			sb.setLength(0);
		}
		
		System.out.println(totalLines);
	}
	
	public void filterRelStartEnd() {
		StringBuilder sb = new StringBuilder();
		
		int totalLines = 0;
		GlueList<String> lines = TextFileUtils.load(englishOnlyFilePath);
		
		int p = 0;
		for (String s : lines) {
			String[] values = s.split("\t");
			String relation = values[1];
			String startNode = values[2];
			String endNode = values[3];
			
			sb.append(relation + "\t" + startNode + "\t" + endNode + "\n");
			
			totalLines++;
			p++;
			if (p == 5000) {
				System.out.println(relation + "\t" + startNode + "\t" + endNode);
				p = 0;
			}
		}
		
		writeFile(sb.toString(), relationStartEnd);
		System.out.println(totalLines);
	}
	
	public void filterRelStartEndClean() {
		StringBuilder sb = new StringBuilder();
		
		int totalLines = 0;
		GlueList<String> lines = TextFileUtils.load(relationStartEndFinal);
		
		int p = 0;
		for (String s : lines) {
			String[] values = s.split("\t");
			String relation = values[0].split("/")[2];
			String startNode = values[1].split("/")[3];
			String endNode = values[2].split("/")[3];
			
			sb.append(relation + "\t" + startNode + "\t" + endNode + "\n");
			
			totalLines++;
			p++;
			if (p == 5000) {
				System.out.println(relation + "\t" + startNode + "\t" + endNode);
				p = 0;
			}
		}
		
		writeFile(sb.toString(), relationStartEndCleanFinal);
		System.out.println(totalLines);
	}
	
	public void getAllRelations() {
		StringBuilder sb = new StringBuilder();
		
		GlueList<String> lines = TextFileUtils.load(relationStartEndFinal);
		GlueList<String> relations = new GlueList<>();
		
		for (String s : lines) {
			String[] values = s.split("\t")[0].split("/");
			String relation = values.length > 3 ? values[2] + "/" + values[3] : values[2];
			
			if (!relations.contains(relation)) {
				sb.append(relation + "\n");
				relations.add(relation);
			}
		}
		
		System.out.println(sb.toString());
	}
	
	public Graph createGraphFromFile() {
		long start = System.currentTimeMillis();
		Graph graph = new SingleGraph("RelationStartEnd", false, true);
		GlueList<String> lines = TextFileUtils.load(relationStartEndCleanFinal);
		
		for (String s : lines) {
			String[] values = s.split("\t");
			String relation = values[0];
			String startNode = values[1];
			String endNode = values[2];
			
			graph.addEdge(startNode + " " + endNode, startNode, endNode, true)
				.addAttribute("relation", relation);
		}
		
		System.out.println("Graph created in " + ((System.currentTimeMillis() - start) / 1000f) + " seconds.");
		
		return graph;
	}
	
	public void printIsA(String concept) {
		Graph graph = createGraphFromFile();
		Node n = graph.getNode(concept);
		
		for (Edge e : n.getEdgeSet()) {
			if ("IsA".equals(e.getAttribute("relation"))) {
				System.out.println(e.getId());
			}
		}
	}
	
	public void printNeighbors(String concept) {
		Graph graph = createGraphFromFile();
		Iterator<Node> nodes = graph.getNode(concept).getNeighborNodeIterator();
		while (nodes.hasNext()) {
			System.out.println(nodes.next().getId());
		}
	}
	
	public void exportGraphFile() {
		Graph graph = createGraphFromFile();
		FileSinkDGS sink = new FileSinkDGS();
		
		try {
			sink.writeAll(graph, assertionGraphDGSFinal);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void importGraphFile() {
		long start = System.currentTimeMillis();
		FileSourceDGS source = new FileSourceDGS();
		Graph graph = new SingleGraph("RelationStartEnd", false, true);
		try {
			source.addSink(graph);
			source.readAll(assertionGraphDGSFinal);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Vectors loaded in " + ((System.currentTimeMillis() - start) / 1000f) + " seconds.");
		System.out.println(graph.getNodeCount());
		
		for (Edge e : graph.getNode("love").getEdgeSet()) {
			System.out.println(e.getId());
		}
	}
	
	public void writeFile(String data, String outputFilePath) {
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			File file = new File(outputFilePath);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			// true = append file
			fw = new FileWriter(file.getAbsoluteFile(), true);
			bw = new BufferedWriter(fw);

			bw.write(data);

			System.out.println("Done");

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}
		}
	}
}
