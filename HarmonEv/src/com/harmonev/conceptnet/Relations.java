package com.harmonev.conceptnet;

import com.google.common.collect.HashBiMap;


/*
 * TODO: update complete list of relations
 * "/r/Antonym", 
"/r/TranslationOf", 
"/r/AtLocation", 
"/r/Attribute", 
"/r/CapableOf", 
"/r/Causes", 
"/r/CausesDesire", 
"/r/CompoundDerivedFrom", 
"/r/CreatedBy", 
"/r/DefinedAs", 
"/r/DerivedFrom", 
"/r/DesireOf", 
"/r/Desires", 
"/r/Entails", 
"/r/EtymologicallyDerivedFrom", 
"/r/HasA", 
"/r/HasContext", 
"/r/HasFirstSubevent", 
"/r/HasLastSubevent", 
"/r/HasPrerequisite", 
"/r/HasProperty", 
"/r/HasSubevent", 
"/r/InstanceOf", 
"/r/IsA", 
"/r/LocatedNear", 
"/r/LocationOfAction", 
"/r/MadeOf", 
"/r/MemberOf", 
"/r/MotivatedByGoal", 
"/r/NotCapableOf", 
"/r/NotDesires", 
"/r/NotHasA", 
"/r/NotHasProperty", 
"/r/NotIsA", 
"/r/NotMadeOf", 
"/r/NotUsedFor", 
"/r/ObstructedBy", 
"/r/PartOf", 
"/r/ReceivesAction", 
"/r/RelatedTo", 
"/r/SimilarSize", 
"/r/SimilarTo", 
"/r/SymbolOf", 
"/r/Synonym", 
"/r/UsedFor", 
"/r/dbpedia/field", 
"/r/dbpedia/genre", 
"/r/dbpedia/influenced", 
"/r/dbpedia/influencedBy", 
"/r/dbpedia/knownFor", 
"/r/dbpedia/languageFamily", 
"/r/dbpedia/mainInterest", 
"/r/dbpedia/notableIdea", 
"/r/dbpedia/spokenIn", 
"/r/wordnet/adjectivePertainsTo", 
"/r/wordnet/adverbPertainsTo", 
"/r/wordnet/participleOf", 
"/r/NotCauses" 
 */
public enum Relations {
	RELATED_TO,
	IS_A,
	PART_OF,
	MEMBER_OF,
	HAS_A,
	USED_FOR,
	CAPABLE_OF,
	AT_LOCATION,
	CAUSES,		
	HAS_SUBEVENT,
	HAS_FIRST_SUBEVENT,
	HAS_LAST_SUBEVENT,
	HAS_PREREQUISITE,
	HAS_PROPERTY,
	MOTIVATED_BY_GOAL,
	OBSTRUCTED_BY,
	DESIRES,
	CREATED_BY,
	SYNONYM,
	ANTONYM,
	DERIVED_FROM,
	TRANSLATION_OF,
	DEFINED_AS,
	SYMBOL_OF;
	
	private static final HashBiMap<Relations, String> MAP = HashBiMap.create(24);
	private static final HashBiMap<Relations, String> DESC_MAP = HashBiMap.create(24);
	
	static {
		MAP.put(RELATED_TO, "/r/RelatedTo");
		MAP.put(IS_A, "/r/IsA");
		MAP.put(PART_OF, "/r/PartOf");
		MAP.put(MEMBER_OF, "/r/MemberOf");
		MAP.put(HAS_A, "/r/HasA");
		MAP.put(USED_FOR, "/r/UsedFor");
		MAP.put(CAPABLE_OF, "/r/CapableOf");
		MAP.put(AT_LOCATION, "/r/AtLocation");
		MAP.put(CAUSES, "/r/Causes");
		MAP.put(HAS_SUBEVENT, "/r/HasSubevent");
		MAP.put(HAS_FIRST_SUBEVENT, "/r/HasFirstSubevent");
		MAP.put(HAS_LAST_SUBEVENT, "/r/HasLastSubevent");
		MAP.put(HAS_PREREQUISITE, "/r/HasPrerequisite");
		MAP.put(HAS_PROPERTY, "/r/HasProperty");
		MAP.put(MOTIVATED_BY_GOAL, "/r/MotivatedByGoal");
		MAP.put(OBSTRUCTED_BY, "/r/ObstructedBy");
		MAP.put(DESIRES, "/r/Desires");
		MAP.put(CREATED_BY, "/r/CreatedBy");
		MAP.put(SYNONYM, "/r/Synonym");
		MAP.put(ANTONYM, "/r/Antonym");
		MAP.put(DERIVED_FROM, "/r/DerivedFrom");
		MAP.put(TRANSLATION_OF, "/r/TranslationOf");
		MAP.put(DEFINED_AS, "/r/DefinedAs");
		MAP.put(SYMBOL_OF, "/r/SymbolOf");
		
		DESC_MAP.put(RELATED_TO, "The most general relation. There is some positive relationship between A and B, but ConceptNet can't determine what that relationship is based on the data. This was called \"ConceptuallyRelatedTo\" in ConceptNet 2 through 4.");
		DESC_MAP.put(IS_A, "A is a subtype or a specific instance of B; every A is a B. (We do not make the type-token distinction, because people don't usually make that distinction.) This is the hyponym relation in WordNet.");
		DESC_MAP.put(PART_OF, "A is a part of B. This is the part meronym relation in WordNet.");
		DESC_MAP.put(MEMBER_OF, "A is a member of B; B is a group that includes A. This is the member meronym relation in WordNet.");
		DESC_MAP.put(HAS_A, "B belongs to A, either as an inherent part or due to a social construct of possession. HasA is often the reverse of PartOf.");
		DESC_MAP.put(USED_FOR, "A is used for B; the purpose of A is B.");
		DESC_MAP.put(CAPABLE_OF, "Something that A can typically do is B.");
		DESC_MAP.put(AT_LOCATION, "A is a typical location for B, or A is the inherent location of B. Some instances of this would be considered meronyms in WordNet.");
		DESC_MAP.put(CAUSES, "A and B are events, and it is typical for A to cause B.");
		DESC_MAP.put(HAS_SUBEVENT, "A and B are events, and B happens as a subevent of A.");
		DESC_MAP.put(HAS_FIRST_SUBEVENT, "A is an event that begins with subevent B.");
		DESC_MAP.put(HAS_LAST_SUBEVENT, "A is an event that concludes with subevent B.");
		DESC_MAP.put(HAS_PREREQUISITE, "In order for A to happen, B needs to happen; B is a dependency of A.");
		DESC_MAP.put(HAS_PROPERTY, "A has B as a property; A can be described as B.");
		DESC_MAP.put(MOTIVATED_BY_GOAL, "Someone does A because they want result B; A is a step toward accomplishing the goal B.");
		DESC_MAP.put(OBSTRUCTED_BY, "A is a goal that can be prevented by B; B is an obstacle in the way of A.");
		DESC_MAP.put(DESIRES, "A is a conscious entity that typically wants B. Many assertions of this type use the appropriate language's word for \"person\" as A.");
		DESC_MAP.put(CREATED_BY, "B is a process that creates A.");
		DESC_MAP.put(SYNONYM, "A and B have very similar meanings. This is the synonym relation in WordNet as well.");
		DESC_MAP.put(ANTONYM, "A and B are opposites in some relevant way, such as being opposite ends of a scale, or fundamentally similar things with a key difference between them. Counterintuitively, two concepts must be quite similar before people consider them antonyms. This is the antonym relation in WordNet as well.");
		DESC_MAP.put(DERIVED_FROM, "A is a word or phrase that appears within B and contributes to B's meaning.");
		DESC_MAP.put(TRANSLATION_OF, "A and B are concepts (or assertions) in different languages, and overlap in meaning in such a way that they can be considered translations of each other. (This cannot, of course be taken as an exact equivalence.)");
		DESC_MAP.put(DEFINED_AS, "A and B overlap considerably in meaning, and B is a more explanatory version of A. (This is similar to TranslationOf, but within one language.)");
	}
	
	public String getValue() {
		return getValue(this);
	}
	
	public static String getValue(Relations rel) {
		return MAP.get(rel);
	}
	
	public static Relations getRelation(String val) {
		return MAP.inverse().get(val);
	}
	
	public static String getRelationDescription(Relations rel) {
		return DESC_MAP.get(rel);
	}
}
