package com.harmonev.conceptnet;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.harmonev.VectorSearch;

public class CNetVectors {
	public static int LINE_COUNT = 426572;
	private String rawFilePath = "E:/ConceptNet/data/conceptnet-numberbatch-201609_en_main.txt";
	private String serializedVectors = "E:/ConceptNet/data/SerializedVectors";
	private String serializedHashMap = "E:/ConceptNet/data/SerializedVectorsHashMap";
	private ArrayList<String> vectors = new ArrayList<String>(LINE_COUNT);
	private HashMap<String, Integer> map = new HashMap<String, Integer>(LINE_COUNT);
	private boolean loaded = false;
	private int numThreads = 4;
	private Comparator<Data> comparator = new Comparator<Data>() {

		@Override
		public int compare(Data o1, Data o2) {
			if (o1.sum < o2.sum) {
				return -1;
			} else if (o1.sum > o2.sum) {
				return 1;
			}
			
			return 0;
		}
	};
	
	public CNetVectors() {
	}
	
	public CNetVectors(int numThreads) {
		this.numThreads = numThreads;
	}
	
	// use when loading from raw text file
	// TODO use TextFileUtils methods
	public void load() {
//		long start = System.currentTimeMillis();
//		System.out.println("Loading vectors...");
		try (BufferedReader br = new BufferedReader(new FileReader(rawFilePath))) {
		    String line;
		    int index = 0;
		    while ((line = br.readLine()) != null) {
		    	vectors.add(line);
		    	map.put(line.split(" ")[0], index);
		    	index++;
		    }
		    
		    loaded = true;
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
//		System.out.println("Loading finished in " + ((System.currentTimeMillis() - start) / 1000f) + " seconds.");
	}
	
//	@SuppressWarnings("unchecked")
//	public void load() {
//		long start = System.currentTimeMillis();
//		System.out.println("Loading vectors and hashmap...");
//		try {
//			FileInputStream fis = new FileInputStream(serializedVectors);
//			ObjectInputStream ois = new ObjectInputStream(fis);
//			vectors = (ArrayList<String>) ois.readObject();
//			ois.close();
//			fis.close();
//			
//			fis = new FileInputStream(serializedHashMap);
//			ois = new ObjectInputStream(fis);
//			map = (HashMap<String, Integer>) ois.readObject();
//			ois.close();
//			fis.close();
//		} catch (IOException ioe) {
//			ioe.printStackTrace();
//			return;
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
//		System.out.println("Loading finished in " + ((System.currentTimeMillis() - start) / 1000f) + " seconds.");
//	}
	
	public String getVector(String concept) {
		String vector = getRawVector(concept);
		return vector == null ? null : vector.substring(concept.length() + 1);
	}
	
	public String getRawVector(String concept) {
		Integer index = map.get(concept);
		return index == null ? null : vectors.get(index);
	}
	
	public float[] getVectorFloats(String concept) {
//		long start = System.currentTimeMillis();
//		System.out.println("Getting vector floats...");
		String vector = getVector(concept);
		if (vector == null) {
			return null;
		}
		
		String[] values = vector.split(" ");
		float[] floats = new float[values.length];
		
		for (int i = 0; i < values.length; i++) {
			floats[i] = Float.parseFloat(values[i]);
		}
//		System.out.println("Got vector floats in " + (System.currentTimeMillis() - start) + " milliseconds.");
		return floats;
	}
	
	public boolean isLoaded() {
		return loaded;
	}
	
	public Set<String> getAllVectorString() {
		return map.keySet();
	}
	
	public String getVector(int index) {
		return vectors.get(index).split(" ")[0];
	}
	
	public float getDistance(String concept1, String concept2) {
		return getDistance(getVectorFloats(concept1), getVectorFloats(concept2));
	}
	
	public static float getDistance(float[] vector1, float[] vector2) {
//		long start = System.currentTimeMillis();
//		System.out.println("Getting distance...");
		float sum = 0f;
		for (int i = 0; i < vector1.length; i ++) {
			sum += Math.pow(vector2[i] - vector1[i], 2);
		}
		
//		System.out.println("Got distance in " + (System.currentTimeMillis() - start) + " milliseconds.");
		return (float) Math.sqrt(sum);
	}
	
	public void serializeVectors() {
		try {
			FileOutputStream fos = new FileOutputStream("E:/ConceptNet/data/SerializedVectors");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(vectors);
			oos.close();
			fos.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public void serializeHashMap() {
		try {
			FileOutputStream fos = new FileOutputStream("E:/ConceptNet/data/SerializedVectorsHashMap");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(map);
			oos.close();
			fos.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public ArrayList<Data> getClosestConcepts(ArrayList<String> concepts) {
		long start = System.currentTimeMillis();
		System.out.println("Multithreading get...");
		ExecutorService pool = Executors.newFixedThreadPool(numThreads);
		Set<Future<ArrayList<Data>>> set = new HashSet<Future<ArrayList<Data>>>();
		int partition = vectors.size() / numThreads;
		for (int i = 0; i < numThreads; i++) {
			Runner callable = new Runner(i * partition, i == numThreads - 1 ? vectors.size() : (i + 1) * partition, concepts);
			Future<ArrayList<Data>> future = pool.submit(callable);
			set.add(future);
		}
		
		ArrayList<Data> data = new ArrayList<Data>(vectors.size());
		for (Future<ArrayList<Data>> future : set) {
			try {
				data.addAll(future.get());
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		Collections.sort(data, comparator);
		System.out.println(data.size() + " " + vectors.size());
		System.out.println("Finished getting distances in " + (System.currentTimeMillis() - start) + " milliseconds.");
		return data;
	}
	
	private class Runner implements Callable<ArrayList<Data>> {
		private int startIndex, endIndex;
		private ArrayList<String> concepts;
		
		public Runner(int startIndex, int endIndex, ArrayList<String> concepts) {
			this.startIndex = startIndex;
			this.endIndex = endIndex;
			this.concepts = concepts;
		}

		@Override
		public ArrayList<Data> call() throws Exception {
			ArrayList<Data> data = new ArrayList<Data>(endIndex - startIndex);
			for (int i = startIndex; i < endIndex; i++) {
				String s = getVector(i);
				if (concepts.contains(s)) {
					continue;
				}
				
				float[] distances = new float[concepts.size()];
				for (int j = 0; j < concepts.size(); j++) {
					distances[j] = getDistance(s, concepts.get(j));
				}
				
				data.add(new Data(s,distances));
			}
			return data;
		}
	}
	
	public static class Data {
		public String concept;
		public float[] distances;
		public float sum = 0f;
		
		public Data(String concept, float[] distances) {
			this.concept = concept;
			this.distances = distances;
			
			for (float f : distances) {
				this.sum += f;
			}
		}
	}
}
