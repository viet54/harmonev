package com.harmonev.conceptnet.uri;

import com.harmonev.conceptnet.ConceptNetType;
import com.harmonev.conceptnet.Language;

import edu.mit.jwi.item.POS;

public abstract class UriHierarchy {
	protected String uri;
	protected ConceptNetType type;
	protected Language lang;
	protected String concept;
	protected String cleanedConcept;
	protected POS pos;
	protected String sense;
	protected String cleanedSense;
	
	protected UriHierarchy() {
	}
	
	public UriHierarchy(ConceptNetType type) {
		this.type = type;
	}

	public abstract String getURI();
	
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public ConceptNetType getType() {
		return type;
	}

	public Language getLang() {
		return lang;
	}

	public void setLang(Language lang) {
		this.lang = lang;
	}

	public String getConceptText() {
		return concept;
	}

	public void setConcept(String concept) {
		this.concept = concept;
	}

	public String getCleanedConcept() {
		return cleanedConcept;
	}

	public void setCleanedConcept(String cleanedConcept) {
		this.cleanedConcept = cleanedConcept;
	}

	public POS getPos() {
		return pos;
	}

	public void setPos(POS pos) {
		this.pos = pos;
	}

	public String getSense() {
		return sense;
	}

	public void setSense(String sense) {
		this.sense = sense;
	}

	public String getCleanedSense() {
		return cleanedSense;
	}

	public void setCleanedSense(String cleanedSense) {
		this.cleanedSense = cleanedSense;
	}
	
	protected static void parseConcept(UriHierarchy concept) {
		String conceptURI = concept.uri;
		
		if (conceptURI == null) {
			throw new NullPointerException("Concept URI can't null.");
		}
		
		String t = conceptURI.substring(0, 2);
		if (t.equals(ConceptNetType.getValue(ConceptNetType.A))) {
			concept.type = ConceptNetType.A;
		} else if (t.equals(ConceptNetType.getValue(ConceptNetType.C))) {
			concept.type = ConceptNetType.C;
		} else if (t.equals(ConceptNetType.getValue(ConceptNetType.D))) {
			concept.type = ConceptNetType.D;
		} else if (t.equals(ConceptNetType.getValue(ConceptNetType.E))) {
			concept.type = ConceptNetType.E;
		} else if (t.equals(ConceptNetType.getValue(ConceptNetType.L))) {
			concept.type = ConceptNetType.L;
		} else if (t.equals(ConceptNetType.getValue(ConceptNetType.R))) {
			concept.type = ConceptNetType.R;
		} else if (t.equals(ConceptNetType.getValue(ConceptNetType.S))) {
			concept.type = ConceptNetType.S;
		} else if (t.equals(ConceptNetType.getValue(ConceptNetType.AND))) {
			concept.type = ConceptNetType.AND;
		} else if (t.equals(ConceptNetType.getValue(ConceptNetType.OR))) {
			concept.type = ConceptNetType.OR;
		} else {
			throw new IllegalArgumentException("ConceptNet type is undefined: " + conceptURI.substring(0, 3));
		}
		
		String[] segments = conceptURI.split("/");
		
		concept.setLang(Language.getLanguage(segments[2]));
		concept.setConcept(segments[3]);
		concept.setCleanedConcept(concept.getConceptText() != null ? concept.getConceptText().replaceAll("_", " ") : null);
		concept.setPos((segments.length > 4 && !segments[4].isEmpty()) ? POS.getPartOfSpeech(segments[4].charAt(0)) : null);
		concept.setSense((segments.length > 5 && !segments[5].isEmpty()) ? segments[5] : null);
		concept.setCleanedSense(concept.getSense() != null ? concept.getSense().replaceAll("_", " ") : null);
	}
}
