package com.harmonev.conceptnet.uri;

import com.harmonev.conceptnet.ConceptNetType;
import com.harmonev.conceptnet.Language;

public class ConceptURI extends UriHierarchy {

	private ConceptURI () {
	}
	
	public ConceptURI(ConceptNetType type, Language lang, String concept) {
		super(type);
		this.lang = lang;
		this.concept = concept;
	}
	
	public ConceptURI(ConceptNetType type, String concept) {
		this(type, Language.EN, concept);
	}
	
	public ConceptURI(String concept) {
		this(ConceptNetType.C, concept);
	}
	
	@Override
	public String getURI() {
		if (uri != null) {
			return uri;
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append(type.getValue());
			sb.append("/" + lang.getValue());
			sb.append("/" + concept);
			if (pos != null) {
				sb.append("/" + pos.getTag());
			}
			if (sense != null) {
				sb.append("/" + sense);
			}
			return sb.toString();
		}
	}
	
	public static ConceptURI parseURI(String uri) {
		ConceptURI c = new ConceptURI();
		c.uri = uri;
		parseConcept(c);
		return c;
	}
}
