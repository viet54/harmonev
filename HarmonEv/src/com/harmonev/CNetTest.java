package com.harmonev;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import com.harmonev.conceptnet.ConceptNetAPI;
import com.harmonev.conceptnet.Relations;
import com.harmonev.conceptnet.models.CNetEdge;
import com.harmonev.conceptnet.models.Lookup;
import com.harmonev.conceptnet.uri.ConceptURI;

public class CNetTest {

	public static void main(String[] args) throws ClientProtocolException, IOException {
		ConceptURI c = new ConceptURI("water");
		Lookup lookup = ConceptNetAPI.getSearch(Relations.RELATED_TO, "start", c.getURI(), 2);
//		Lookup lookup = ConceptNetAPI.getLookup(c);
		for (CNetEdge edge : lookup.getEdges()) {
			System.out.print(edge.getStart().getConceptText() + " ");
			System.out.print(edge.getRel() + " ");
			System.out.print(edge.getEnd().getConceptText() + ": ");
			System.out.print(edge.getStart().getCleanedSense());
			System.out.print("\n");
			System.out.println(edge.getJson());
		}
		
//		System.out.println(lookup.getJson());
		
//		System.out.println(ConceptNetAPI.getUri(Language.EN, "ground beef").getConcept());
	}
}

