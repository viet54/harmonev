package com.harmonev;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

import util.Stemmer;
import util.Stemmer.StemmerType;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.harmonev.NTree.RelationDirection;
import com.harmonev.conceptnet.CNetVectors;
import com.harmonev.conceptnet.ConceptNetAPI;
import com.harmonev.conceptnet.Language;
import com.harmonev.conceptnet.Relations;
import com.harmonev.conceptnet.models.CNetEdge;
import com.harmonev.conceptnet.models.Lookup;
import com.harmonev.conceptnet.uri.ConceptURI;
import com.harmonev.conceptnet.uri.UriHierarchy;

public class LegacySearch {
	private Config config;
	private String[] words;
	ArrayList<NTree<String>> trees = new ArrayList<NTree<String>>();
	private Graph graph;
	private static CNetVectors vectors = new CNetVectors();
	
	private enum SearchMethod {Lookup, Hybrid}
	private enum Attribute {Start, End}
	
	private static String styleSheet =
			"graph {" +
            "	text-size: 20;" +
            "}" +
			"node {" +
            "	fill-color: black;" +
            "	text-size: 16;" +
            "}" +
            "node.root {" +
            "	fill-color: red;" +
            "}" +
            "node.intersection {" +
            "	fill-color: blue;" +
            "}" +
            "edge {" +
            "	text-size: 16;" +
            "}" +
            "edge.common {" +
            "	fill-color: purple;" +
            "}";
	
	public LegacySearch(String text) {
		config = new Config();
		words = text.split(" ");
		
		for (String word : words) {
			trees.add(new NTree<String>(word.contains("_"), word));
		}
	}
	
	public void searchLookup(int numLayers, int nodesPerLayer, boolean byConcept, int limit) throws ClientProtocolException, IOException {
		search(SearchMethod.Lookup, numLayers, nodesPerLayer, byConcept, limit);
	}
	
	public void searchHybrid(int numLayers, int limit) throws ClientProtocolException, IOException {
		search(SearchMethod.Hybrid, numLayers, -1, false, limit);
		// TODO remove the leafs from intersecting nodes since it's redundant?
		// we don't care if the leafs will create another path back to the roots
		// because we will build the concept space around this intersecting concept
		// to do the ranking in the end
	}
	
	private void search(SearchMethod method, int numLayers, int nodesPerLayer, boolean byConcept, int limit) throws ClientProtocolException, IOException {
		Queue<Tuple<NTree<String>.TreeNode, NTree<String>>> currentQueue = new LinkedList<Tuple<NTree<String>.TreeNode, NTree<String>>>();
		Queue<Tuple<NTree<String>.TreeNode, NTree<String>>> nextQueue = new LinkedList<Tuple<NTree<String>.TreeNode, NTree<String>>>();
		Queue<Tuple<NTree<String>.TreeNode, NTree<String>>> temp;
		
		for (NTree<String> t : trees) {
			currentQueue.add(new Tuple<NTree<String>.TreeNode, NTree<String>>(t.getRoot(), t));
		}
		
		int iteration = 0;
		while (iteration < numLayers) {
			Tuple<NTree<String>.TreeNode, NTree<String>> parent = currentQueue.remove();
			ArrayList<NTree<String>.TreeNode> children = null;
			
			// TODO see if we can make this duplicate check more efficient
			// because we're calling getIntersection() each time
			if (!getIntersection().contains(parent.value1.getValue()) || parent.value1 == parent.value2.getRoot()) {
				switch (method) {
				case Lookup:
					children = traverseByLookup(parent.value1, parent.value2, nodesPerLayer, byConcept);
					break;
				case Hybrid:
					children = traverseByHybrid(parent.value1, parent.value2, limit);
					break;
				}
				
				for (NTree<String>.TreeNode child : children) {
					nextQueue.add(new Tuple<NTree<String>.TreeNode, NTree<String>>(child, parent.value2));
				}
			}
			
			if (currentQueue.isEmpty()) {
				temp = nextQueue;
				nextQueue = currentQueue;
				currentQueue = temp;
				nextQueue.clear();
				iteration++;
			}
		}
		
		buildGraph(numLayers);
	}
	
	private ArrayList<NTree<String>.TreeNode> traverseByLookup(NTree<String>.TreeNode parent, NTree<String> tree, int nodesPerLayer, boolean byConcept) throws ClientProtocolException, IOException {
		Lookup lookup = ConceptNetAPI.getLookup(new ConceptURI(parent.getValue()));
		CNetEdge[] edges = lookup.getEdges();
		
		int i = 0;
		for (int j = 0; j < edges.length && i < nodesPerLayer; j++) {
			if (edges[j].getStart().getLang() != Language.EN) {
				continue;
			}
			
			String concept = null;
			if (byConcept) {
				String conceptStart = edges[j].getStart().getConceptText();
				String conceptEnd = edges[j].getEnd().getConceptText();
				concept = (conceptStart != null && !parent.getValue().equals(conceptStart)) ? conceptStart : (conceptEnd != null ? conceptEnd : null);
			} else {
				concept = edges[j].getStart().getCleanedSense() != null ? edges[j].getStart().getCleanedSense() : edges[j].getStart().getCleanedConcept();
			}
			
			if (concept != null) {
				for (String child : Stemmer.getWordsOfInterest(concept, StemmerType.ConceptNet, byConcept)) {
					tree.add(byConcept, parent.getValue(), child, concept, null, RelationDirection.Normal, edges[j].getWeight());
				}
				
				i++;
			}
		}
		
		return tree.get(parent.getValue()).getChildren();
	}
	
	// TODO stop searching when we have intersection because we will have duplicate leaves
	// and this will cut down search time by having less leaves
	// we can do this by alternating trees for each layer
	// and checking intersection
	// TODO we can also cut down search time by only adding concepts to the tree
	// that has a weight that's above a certain threshold
	private ArrayList<NTree<String>.TreeNode> traverseByHybrid(NTree<String>.TreeNode parent, NTree<String> tree, int limit) throws ClientProtocolException, IOException {
		ConceptURI c = new ConceptURI(parent.getValue());
		
		for (Relations rel : config.SEARCH_RELATIONS) {
//			addByRelation(parent, tree, rel, Attribute.Start, c.getURI(), limit, false);
			addByRelation(parent, tree, rel, Attribute.End, c.getURI(), limit, true);
		}
		
		return tree.get(parent.getValue()).getChildren();
	}
	
	private void addByRelation(NTree<String>.TreeNode parent, NTree<String> tree, Relations relation, Attribute attribute, String uri, int limit, boolean byConcept) {
		Lookup lookup = ConceptNetAPI.getSearch(relation, attribute == Attribute.Start ? "start" : "end", uri, limit);
		CNetEdge[] edges = lookup.getEdges();
		
		// TODO use while loop instead of for loop and if the conditions aren't met
		// search again to fulfill the limit
		for (int j = 0; j < edges.length; j++) {
			// we do want to swap start and end because we want to find the other end of the search
			// so if we search for attribute "start", we want to find what's at the "end"
			UriHierarchy hierarchy = attribute == Attribute.Start ? edges[j].getEnd() : edges[j].getStart();
			String concept = hierarchy.getConceptText() != null ? hierarchy.getConceptText() : hierarchy.getConceptText();
			
			// sometimes we get an edge from a DEFINED_AS relation that returns a "plural of..." definition
			// that we want to ignore
			if (hierarchy.getLang() != Language.EN || concept.contains("plural")) {
				continue;
			}
			
			
			if (concept != null) {
				for (String child : Stemmer.getWordsOfInterest(concept, StemmerType.ConceptNet, byConcept)) {
					tree.add(byConcept, parent.getValue(), child, concept, edges[j].getRel(), attribute == Attribute.Start ? 
							RelationDirection.Normal : RelationDirection.Reverse, edges[j].getWeight());
				}
			}
		}
		
	}
	
	public void printTrees() {
		for (NTree<String> t : trees) {
			System.out.println(t.toString());
		}
		
		for (NTree<String> t : trees) {
			for (String s : t.getAllRelation()) {
				System.out.println(s);
			}
		}
		
		System.out.println();
	}
	
	private void buildGraph(int numLayers) {
		graph = new SingleGraph("graph");
		graph.addAttribute("ui.stylesheet", styleSheet);
		
		List<String> rootWords = Arrays.asList(words);
		Set<String> intersection = getIntersection();
		
		for (NTree<String> t : trees) {
			Queue<NTree<String>.TreeNode> currentQueue = new LinkedList<NTree<String>.TreeNode>();
			Queue<NTree<String>.TreeNode> nextQueue = new LinkedList<NTree<String>.TreeNode>();
			Queue<NTree<String>.TreeNode> temp;
			
			currentQueue.add(t.getRoot());
			// TODO show only the intersection paths and concepts not everything in the trees
			while (!currentQueue.isEmpty()) {
				NTree<String>.TreeNode node = currentQueue.remove();
				
				if (graph.getNode(node.getValue()) == null) {
					graph.addNode(node.getValue()).addAttribute("ui.label", node.getValue());
					
					if (rootWords.contains(node.getValue())) {
						graph.getNode(node.getValue()).setAttribute("ui.class", "root");
					} else if (intersection.contains(node.getValue()) && !rootWords.contains(node.getValue())) {
						graph.getNode(node.getValue()).setAttribute("ui.class", "intersection");
					}
					// TODO also need to highlight nodes that are along the path, not just the intersecting node
					// because any node along the path is part of the connecting bridge
				}
				
				// adding edges from the parent-child relation from the search tree
				if (node.getParent() != null) {
					RelationDirection dir = node.getDirection();
					String from = dir == RelationDirection.Normal ? node.getParent().getValue() : node.getValue();
					String to = dir == RelationDirection.Normal ? node.getValue() : node.getParent().getValue();
					String id = from + " " + to;
					
					if (graph.getEdge(id) == null) {
						graph.addEdge(id, from, to, true);
						graph.getEdge(id).addAttribute("edgeWeight", node.getWeightToParent());
						graph.getEdge(id).addAttribute("ui.label", node.getRelationshipToParent() + " " + node.getWeightToParent());
					}

				}
				
				if (!node.getChildren().isEmpty()) {
					nextQueue.addAll(node.getChildren());
				}
				
				if (currentQueue.isEmpty()) {
					temp = nextQueue;
					nextQueue = currentQueue;
					currentQueue = temp;
					nextQueue.clear();
				}
			}
			
			// adding edges from all the other relation skipped by the search
			for (String rel : t.getAllRelation()) {
				String[] s = rel.split(" ");
				String parent = s[0];
				String child = s[1];
				String weight = s[2];
				
				String relNormal = parent + " " + child;
				String relReverse = child + " " + parent;
				
				if (graph.getEdge(relNormal) == null && graph.getEdge(relReverse) == null) {
					graph.addEdge(relNormal, parent, child, true);
					graph.getEdge(relNormal).addAttribute("edgeWeight", Float.parseFloat(weight));
					graph.getEdge(relNormal).addAttribute("ui.label", weight);
				}
			}
			
			for (String i : intersection) {
				NTree<String>.TreeNode node = t.get(i);
				NTree<String>.TreeNode parent = node.getParent();
				
				while (parent != null) {
					String idNormal = node.getValue() + " " + parent.getValue();
					String idReverse = parent.getValue() + " " + node.getValue();
					
					org.graphstream.graph.Edge edge = graph.getEdge(idNormal) != null ? graph.getEdge(idNormal) : graph.getEdge(idReverse);
					edge.setAttribute("ui.class", "common");
					
					node = parent;
					parent = node.getParent();
				}
			}
		}
		
		// removing dead end nodes so we can see clearer on the graph
		// edge case when there's no intersecting concept it will remove all edges and
		// leave one node
		boolean removeDeadEnd = (intersection.size() >= rootWords.size() || numLayers == 1) && !intersection.isEmpty();
		while (removeDeadEnd) {
			boolean deadEndFound = false;
			for (org.graphstream.graph.Node node : graph.getNodeSet()) {
				if (node.getEdgeSet().size() == 1) {
					graph.removeNode(node);
					deadEndFound = true;
					System.out.println("removing deadend");
				}
			}
			
			if (!deadEndFound) {
				System.out.println("no more deadend");
				break;
			}
		}
	}
	
	public void displayGraph() {
		graph.display();
	}
	
	public Graph getGraph() {
		return graph;
	}
	
	public void printRelationship() {
		Set<String> intersection = getIntersection();
		for (String s : intersection) {
			String rel = "";
			for (NTree<String> t : trees) {
				NTree<String>.TreeNode root = t.getRoot();
				NTree<String>.TreeNode child = t.get(s);
				NTree<String>.TreeNode parent = child.getParent();
				
				rel = child.getValue() + rel;
				
				while (parent != root && parent != null) {
					rel = parent.getValue() + " " + rel;
					child = parent;
					parent = child.getParent();
				}
				
				rel = "\n" + root.getValue() + " " + rel;
			}
			System.out.println(rel);
		}
	}
	
	public Set<String> getIntersection() {
		if (trees.size() < 2) {
			return null;
		}
		
		Set<String> intersection = Sets.intersection(trees.get(0).getMap().keySet(), trees.get(1).getMap().keySet());
		
		for (int i = 2; i < trees.size(); i++) {
			intersection = Sets.intersection(intersection, trees.get(i).getMap().keySet());
		}
		
		return intersection;
	}
	
	public ArrayList<NTree<String>> getTrees() {
		return trees;
	}
	
	/////////////////////////////////////////////////////////////
	/*New Search Code Begin*/
	/////////////////////////////////////////////////////////////
	
	public static Lookup lookUp(String concept, int limit) {
		return Strings.isNullOrEmpty(concept) ? null : ConceptNetAPI.getLookup(new ConceptURI(concept), limit);
	}
	
	public static Lookup lookUp(String concept) {
		return lookUp(concept, 50);
	}
	
	public static boolean isRelated(String firstConcept, String secondConcept) {
		return !getRelations(firstConcept, secondConcept, 1).isEmpty();
	}
	
	public static void evolve(String startConcept, String endConcept, int topConceptCount, int searchLimit) {
		Graph graph = new SingleGraph("concept");
		graph.addAttribute("ui.stylesheet", styleSheet);
		graph.addNode(startConcept).addAttribute("ui.label", startConcept);
		graph.getNode(startConcept).setAttribute("ui.class", "root");
		graph.addNode(endConcept).addAttribute("ui.label", endConcept);
		graph.getNode(endConcept).setAttribute("ui.class", "root");
		evolve(graph, startConcept, endConcept, topConceptCount, searchLimit, 2, 0);
		
		boolean removeDeadEnd = true;
		while (removeDeadEnd) {
			boolean deadEndFound = false;
			for (org.graphstream.graph.Node node : graph.getNodeSet()) {
				if (node.getEdgeSet().size() == 1) {
					graph.removeNode(node);
					deadEndFound = true;
				}
			}
			
			if (!deadEndFound) {
				break;
			}
		}
		
		
		graph.display();
	}
	
	public static void evolve(Graph graph, String startConcept, String endConcept, int topConceptCount, int searchLimit, int layerLimit, int currentLayer) {
		if (currentLayer >= layerLimit) {
			return;
		}
		List<String> result = getTopClosestConcepts(startConcept, endConcept, topConceptCount, searchLimit);
		
		for (String s : result) {
			String end = s.split(" ")[1];
			if (graph.getNode(end) == null) {
				graph.addNode(end).addAttribute("ui.label", end);
				graph.addEdge(startConcept + " " + end, startConcept, end);
				if (isRelated(end, endConcept)) {
					graph.addEdge(end + " " + endConcept, end, endConcept);
				} else {
					evolve(graph, end, endConcept, topConceptCount, searchLimit, layerLimit, currentLayer + 1);
				}
			}
		}
	}
	
	public static SearchResultWrapper getRelations(String firstConcept, String secondConcept, int limit) {
		SearchResultWrapper searchResult = new SearchResultWrapper(limit);
		
		Lookup lookupNormal = searchRelations(firstConcept, secondConcept, limit);
		if (lookupNormal != null) {
			for (CNetEdge edge : lookupNormal.getEdges()) {
				if (edge.getRel() != null) {
					searchResult.addRelation(edge.getStart().getConceptText(),
							edge.getEnd().getConceptText(), edge.getRel());
				}
			}
		}
		
		Lookup lookupReversed = searchRelations(secondConcept, firstConcept, limit);
		if (lookupReversed != null) {
			for (CNetEdge edge : lookupReversed.getEdges()) {
				if (edge.getRel() != null) {
					searchResult.addRelation(edge.getStart().getConceptText(),
							edge.getEnd().getConceptText(), edge.getRel());
				}
			}
		}
		
		return searchResult;
	}
	
	private static Lookup searchRelations(String startConcept, String endConcept, int limit) {
		return Strings.isNullOrEmpty(startConcept) || Strings.isNullOrEmpty(endConcept) ?
				null : ConceptNetAPI.getSearch(startConcept, endConcept, limit);
	}
	
	public static Graph graph(String concept, int limit) {
		Graph graph = buildGraph(concept, limit);
		if (graph != null) {
			graph.display();
		}
		return graph;
	}
	
	public static void graph(String concept) {
		graph(concept, 50);
	}
	
	public static void graph(String concept, int limit, boolean withAssociation, boolean multiThread) {
		if (!withAssociation) {
			graph(concept, limit);
			return;
		}
		
		Graph graph = buildGraph(concept, limit);
		if (graph != null) {
			if (multiThread) {
			ArrayList<String> firstConcepts = new ArrayList<String>();
			ArrayList<String> secondConcepts = new ArrayList<String>();
				for (Edge edge : graph.getEachEdge()) {
					String[] concepts = edge.getId().split(" ");
					firstConcepts.add(concepts[0]);
					secondConcepts.add(concepts[1]);
				}
				
				float[] values = associationBetweenTwoConcepts(firstConcepts.toArray(new String[firstConcepts.size()]), secondConcepts.toArray(new String[firstConcepts.size()]));
				
				for (int i = 0; i < firstConcepts.size(); i++) {
					Edge edge = graph.getEdge(firstConcepts.get(i) + " " + secondConcepts.get(i));
					String attr = edge.getAttribute("ui.label") + " | " + values[i];
					edge.changeAttribute("ui.label", attr);
				}
			} else {
				for (Edge edge : graph.getEachEdge()) {
					String[] concepts = edge.getId().split(" ");
					float value = associationBetweenTwoConcepts(concepts[0], concepts[1]);
					String attr = edge.getAttribute("ui.label") + " | " + value;
					edge.changeAttribute("ui.label", attr);
				}
			}
			graph.display();
		}
	}
	
	// TODO
	// just a note, we can search to see if two concepts are connected through
	// http://conceptnet5.media.mit.edu/data/5.4/search?end=/c/en/apple&start=/c/en/fruit
	public static void graphWithVectorDistance(String conceptA, String conceptB, int limit) {
		Graph graph = graph(conceptA, limit);
		Node rootNode = graph.getNode(conceptA);
		
		StringBuilder sb = new StringBuilder();
		ArrayList<String> result = new ArrayList<String>(graph.getNodeSet().size());
		
		for (Node node : graph.getNodeSet()) {
			if (node != rootNode) {
				sb.setLength(0);
				float[] vector1 = getVector().getVectorFloats(node.getId());
				float[] vector2 = getVector().getVectorFloats(conceptB);
				if (vector1 != null && vector2 != null) {
					double distance = CNetVectors.getDistance(vector1, vector2);
					sb.append(distance);
					sb.append(" " + node.getId() + " " + conceptB);
					result.add(sb.toString());
				}
			}
		}
		
		Collections.sort(result.subList(0, result.size()));
		
		for (String s : result) {
			System.out.println(s);
		}
	}
	
	public static ArrayList<String> getClosestConcepts(String startConcept, String endConcept, int searchLimit) {
		LookupResultWrapper lookupResult = simpleLookup(startConcept, searchLimit);
		StringBuilder sb = new StringBuilder();
		ArrayList<String> result = new ArrayList<String>(searchLimit);
		
		for (int i = 0; i < lookupResult.size(); i++) {
			sb.setLength(0);
			float[] vector1 = getVector().getVectorFloats(lookupResult.getChildConcept(i));
			float[] vector2 = getVector().getVectorFloats(endConcept);
			if (vector1 != null && vector2 != null) {
				double distance = CNetVectors.getDistance(vector1, vector2);
				sb.append(distance);
				sb.append(" " + lookupResult.getChildConcept(i) + " " + endConcept);
				result.add(sb.toString());
			}
		}
		
		Collections.sort(result.subList(0, result.size()));
		return result;
	}
	
	public static List<String> getTopClosestConcepts(String startConcept, String endConcept, int topConceptCount, int searchLimit) {
		ArrayList<String> result = getClosestConcepts(startConcept, endConcept, searchLimit);
		return result.subList(0, topConceptCount < result.size() ? topConceptCount : result.size());
	}
	
	private static Graph buildGraph(String concept, int limit) {
		if (concept == null) {
			return null;
		}
		
		LookupResultWrapper lookupResult = simpleLookup(concept, limit);
		Graph graph = new SingleGraph("concept");
		graph.addAttribute("ui.stylesheet", styleSheet);
		graph.addNode(concept).addAttribute("ui.label", concept); // adding root word
		
		for (int i = 0; i < lookupResult.size(); i++) {
			String childConcept = lookupResult.getChildConcept(i);
			String edgeId = lookupResult.getEdgeId(i);
			graph.addNode(childConcept).addAttribute("ui.label", childConcept);
			graph.addEdge(edgeId, lookupResult.getFromConcept(i), lookupResult.getToConcept(i), true);
			graph.getEdge(edgeId).addAttribute("edgeWeight", lookupResult.getWeightValue(i));
			graph.getEdge(edgeId).addAttribute("ui.label", lookupResult.getWeightLabels(i));
		}
		
		return graph;
	}
	
	private static LookupResultWrapper simpleLookup(String concept, int limit) {
		LookupResultWrapper wrapper = new LookupResultWrapper(concept, limit);
		Lookup lookup = lookUp(concept, limit);
		if (lookup != null) {
			for (CNetEdge edge : lookup.getEdges()) {
				boolean relNormal = concept.equals(edge.getStart().getConceptText());
				UriHierarchy uri = relNormal ? edge.getEnd() : edge.getStart();
				String childConcept = uri.getConceptText();
				if (childConcept == null) {
					continue;
				}
				
				if (edge.getStart().getLang() == Language.EN && !childConcept.contains("plural") && !wrapper.contains(childConcept)) {
					wrapper.add(childConcept, relNormal, edge.getWeight(), edge.getRel());
				}
			}
		}
		return wrapper;
	}
	
	private static class LookupResultWrapper {
		private String parentConcept;
		private ArrayList<String> childConcepts;
		private ArrayList<String> edgeIds;
		private ArrayList<Boolean> relationNormal;
		private ArrayList<Float> weights;
		private ArrayList<String> relTypes;
		
		public LookupResultWrapper(String parentConcept, int count) {
			this.parentConcept = parentConcept;
			childConcepts = new ArrayList<>(count);
			edgeIds = new ArrayList<>(count);
			relationNormal = new ArrayList<>(count);
			weights = new ArrayList<>(count);
			relTypes = new ArrayList<>(count);
		}
		
		public void add(String childConcept, boolean relNormal, Float weight, String relType) {
			childConcepts.add(childConcept);
			edgeIds.add(relNormal ? parentConcept + " " + childConcept : childConcept + " " + parentConcept);
			relationNormal.add(relNormal);
			weights.add(weight);
			relTypes.add(relType);
		}
		
		public String getChildConcept(int index) {
			return childConcepts.get(index);
		}
		
		public String getEdgeId(int index) {
			return edgeIds.get(index);
		}
		
		public String getFromConcept(int index) {
			return relationNormal.get(index) ? parentConcept : childConcepts.get(index);
		}
		
		public String getToConcept(int index) {
			return relationNormal.get(index) ? childConcepts.get(index) : parentConcept;
		}
		
		public Float getWeightValue(int index) {
			return weights.get(index);
		}
		
		public String getWeightLabels(int index) {
			return relTypes.get(index) + " " + weights.get(index);
		}
		
		public boolean contains(String childConcept) {
			return childConcepts.contains(childConcept) || parentConcept.equals(childConcept);
		}
		
		public int size() {
			return childConcepts.size();
		}
	}
	
	public static class SearchResultWrapper {
		ArrayList<String> result;
		
		public SearchResultWrapper(int count) {
			this.result = new ArrayList<String>(count);
		}
		
		public void addRelation(String startConcept, String endConcept, String relation) {
			String value = startConcept + " " + relation + " " + endConcept;
			if (!result.contains(value)) {
				result.add(value);
			}
		}
		
		public String getRelation(int index) {
			return result.get(index).split(" ")[1];
		}
		
		public boolean isEmpty() {
			return result.isEmpty();
		}
		
		public ArrayList<String> getResult() {
			return result;
		}
	}
	
	public static String association(String concept) {
		return concept == null ? null : ConceptNetAPI.getAssociation(new ConceptURI(concept)).getJson();
	}
	
	public static String associationTopConcepts(String firstConcept, String secondConcept) {
		if (firstConcept == null || secondConcept == null) {
			return null;
		}
		
		StringBuilder sb = new StringBuilder();
		String[][] similarValues = ConceptNetAPI.getAssociation(new ConceptURI(firstConcept)).getSimilarValues();
		
		sb.append("Concepts similar to 'firstConcept' is:\n");
		for (String[] value : similarValues) {
			String associatedConcept = value[0].split("/")[3];
			System.out.println(associatedConcept + " " + secondConcept);
			sb.append(associatedConcept + " " + secondConcept + " " + associationBetweenTwoConcepts(secondConcept, associatedConcept) + "\n");
		}
		
		return sb.toString();
	}
	
	public static float associationBetweenTwoConcepts(String firstConcept, String secondConcept) {
		return firstConcept == null || secondConcept == null ? 0f : 
			ConceptNetAPI.getAssociationBetweenConcepts(new ConceptURI(firstConcept), new ConceptURI(secondConcept));
	}
	
	public static float[] associationBetweenTwoConcepts(String[] firstConcepts, String[] secondConcepts) {
		ConceptURI[] firstConceptURIs = new ConceptURI[firstConcepts.length];
		ConceptURI[] secondConceptURIs = new ConceptURI[secondConcepts.length];
		for (int i = 0; i < firstConcepts.length; i++) {
			firstConceptURIs[i] = new ConceptURI(firstConcepts[i]);
			secondConceptURIs[i] = new ConceptURI(secondConcepts[i]);
		}
		return ConceptNetAPI.multiGetAssociation(firstConceptURIs, secondConceptURIs);
	}
	
	private static CNetVectors getVector() {
		if (!vectors.isLoaded()) {
			vectors.load();
		}
		
		return vectors;
	}
	
	/////////////////////////////////////////////////////////////
	/*New Search Code End*/
	/////////////////////////////////////////////////////////////
	
	private class Config {
		private Relations[] SEARCH_RELATIONS = 
			{	Relations.RELATED_TO,
				Relations.IS_A,
				Relations.PART_OF,
				Relations.MEMBER_OF,
				Relations.HAS_A,
				Relations.USED_FOR,
				Relations.CAPABLE_OF,
				Relations.AT_LOCATION,
				Relations.CAUSES,		
				Relations.HAS_SUBEVENT,
				Relations.HAS_FIRST_SUBEVENT,
				Relations.HAS_LAST_SUBEVENT,
				Relations.HAS_PREREQUISITE,
				Relations.HAS_PROPERTY,
				Relations.MOTIVATED_BY_GOAL,
				Relations.OBSTRUCTED_BY,
				Relations.DESIRES,
				Relations.CREATED_BY,
				Relations.SYNONYM,
				Relations.ANTONYM,
				Relations.DERIVED_FROM,
				Relations.TRANSLATION_OF,
				Relations.DEFINED_AS,
				Relations.SYMBOL_OF 
				};
		
		private int numLayers;
		private int numNodes;
		private int limit;
		private boolean byConcept;
		
		public Config setSearchRelations(Relations[] sEARCH_RELATIONS) {
			SEARCH_RELATIONS = sEARCH_RELATIONS;
			return this;
		}
		public Config setNumLayers(int numLayers) {
			this.numLayers = numLayers;
			return this;
		}
		public Config setNumNodes(int numNodes) {
			this.numNodes = numNodes;
			return this;
		}
		public Config setLimit(int limit) {
			this.limit = limit;
			return this;
		}
		public Config setByConcept(boolean byConcept) {
			this.byConcept = byConcept;
			return this;
		}
	}
	
	public static class Tuple<T,U> {
		public T value1;
		public U value2;
		
		public Tuple(T value1, U value2) {
			this.value1 = value1;
			this.value2 = value2;
		}
	}
}
