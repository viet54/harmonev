package com.harmonev;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import com.google.gson.internal.LinkedHashTreeMap;

public class NTree<T> {
	private Map<T, TreeNode> map = new LinkedHashTreeMap<T, TreeNode>();
	private TreeNode root;
	private ArrayList<String> allRelation = new ArrayList<String>();
	
	public static enum RelationDirection {Normal, Reverse}

	public NTree(boolean isConcept, T value) {
		root = new TreeNode(isConcept, null, value, null, null, RelationDirection.Normal, 0);
		map.put(value, root);
	}

	public TreeNode getRoot() {
		return root;
	}

	public TreeNode get(T key) {
		return map.get(key);
	}
	
	public boolean add(boolean isConcept, TreeNode parentNode, T childValue, T childSense, T relationshipToParent,
			RelationDirection direction, float weight) {
		String relNormal = parentNode.getValue() + " " + childValue + " " + weight;
		String relReverse = childValue + " " + parentNode.getValue() + " " + weight;
		String rel = direction == RelationDirection.Normal ? relNormal : relReverse;
		
		if (!allRelation.contains(relNormal) && !allRelation.contains(relReverse)) {
			allRelation.add(rel);
		}
		
		if (map.containsKey(childValue)) {
			return false;
		}

		TreeNode node = new TreeNode(isConcept, parentNode, childValue, childSense, relationshipToParent, direction, weight);
		parentNode.children.add(node);
		map.put(childValue, node);
		return true;
	}
	
	public boolean add(boolean isConcept, T parentValue, T childValue, T childSense, T relationshipToParent,
			RelationDirection direction, float weight) {
		return add(isConcept, get(parentValue), childValue, null, relationshipToParent, direction, weight);
	}
	
	public Map<T, TreeNode> getMap() {
		return map;
	}

	public ArrayList<String> getAllRelation() {
		return allRelation;
	}

	public boolean contains(T key) {
		return map.containsKey(key);
	}

	@Override
	public String toString() {
		if (root == null) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		Queue<TreeNode> currentQueue = new LinkedList<TreeNode>();
		Queue<TreeNode> nextQueue = new LinkedList<TreeNode>();
		Queue<TreeNode> temp;

		currentQueue.add(root);

		while (!currentQueue.isEmpty()) {
			TreeNode node = currentQueue.remove();
			sb.append(node.getValue());
			sb.append(" ");

			if (!node.getChildren().isEmpty()) {
				nextQueue.addAll(node.getChildren());
			}

			if (currentQueue.isEmpty()) {
				sb.append("\n");
				temp = nextQueue;
				nextQueue = currentQueue;
				currentQueue = temp;
				nextQueue.clear();
			}
		}
		return sb.toString();
	}

	public class TreeNode {
		private ArrayList<TreeNode> children = new ArrayList<TreeNode>();
		private TreeNode parentNode;
		private boolean isConcept;
		private T childValue;
		private T childSense;
		private T relationshipToParent;
		private RelationDirection direction;
		private float weightToParent;

		public TreeNode(boolean isConcept, TreeNode parentNode, T childValue, T childSense,
				T relationshipToParent, RelationDirection direction, float weight) {
			this.isConcept = isConcept;
			this.childValue = childValue;
			this.parentNode = parentNode;
			this.childSense = childSense;
			this.relationshipToParent = relationshipToParent;
			this.direction = direction;
			this.weightToParent = weight;
		}
		
		public boolean isConcept() {
			return isConcept;
		}

		public TreeNode getParent() {
			return parentNode;
		}
		
		public T getValue() {
			return childValue;
		}

		public T getChildSense() {
			return childSense;
		}

		public T getRelationshipToParent() {
			return relationshipToParent;
		}

		public RelationDirection getDirection() {
			return direction;
		}

		public ArrayList<TreeNode> getChildren() {
			return children;
		}

		public float getWeightToParent() {
			return weightToParent;
		}
	}
}
