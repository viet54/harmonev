package com.harmonev;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import com.harmonev.conceptnet.CNetVectors;

public class VectorSearch {
	private CNetVectors vectors;
	private Comparator<Data> comparator = new Comparator<VectorSearch.Data>() {

		@Override
		public int compare(Data o1, Data o2) {
			if (o1.sum < o2.sum) {
				return -1;
			} else if (o1.sum > o2.sum) {
				return 1;
			}
			
			return 0;
		}
	};

	public VectorSearch() {
		vectors = new CNetVectors();
		
		System.out.println("Loading vectors...");
		long start = System.currentTimeMillis();
		vectors.load();
		System.out.println("Vectors loaded in " + ((System.currentTimeMillis() - start) / 1000f) + " seconds.");
	}
	
	public void search(String concept1, String concept2) {
		LinkedList<Data> data = new LinkedList<Data>();
		float[] float1 = vectors.getVectorFloats(concept1);
		float[] float2 = vectors.getVectorFloats(concept2);
		
		if (float1 == null || float2 == null) {
			System.out.println(String.format(
					"Concept '%s' and/or '%s' not found in vector file.", concept1, concept2));
		}
		
		int size = vectors.getAllVectorString().size() - 1;
		float f = 1f;
		long start = System.currentTimeMillis();
		System.out.println("Getting distances...");
		for (String s : vectors.getAllVectorString()) {
			if (s.equals(concept1) || s.equals(concept2)) {
				continue;
			}
			
			data.add(new Data(s, vectors.getDistance(s, concept1), vectors.getDistance(s, concept2)));
			
//			System.out.println((int) ((f / size) * 100) +"%");
			f++;
		}
		System.out.println("Finished getting distances in " + ((System.currentTimeMillis() - start) / 1000f) + " seconds.");
		
		start = System.currentTimeMillis();
		System.out.println("Sorting...");
		Collections.sort(data, comparator);
		System.out.println("Sorting finished in " + ((System.currentTimeMillis() - start) / 1000f) + " seconds.");
		
		for (int i = 0; i < 500; i++) {
			Data d = data.get(i);
			System.out.println(String.format("%-10s %-10s %-25s %-10s %-10s | Sum: %s",
					d.firstDistance,
					concept1,
					d.concept.toUpperCase(),
					concept2,
					d.secondDistance,
					d.sum));
		}
	}
	
	public static class Data {
		public String concept;
		private float firstDistance;
		private float secondDistance;
		public float sum;
		
		public Data(String concept, float firstDistance, float secondDistance) {
			this.concept = concept;
			this.firstDistance = firstDistance;
			this.secondDistance = secondDistance;
			this.sum = firstDistance + secondDistance;
		}
	}
}
