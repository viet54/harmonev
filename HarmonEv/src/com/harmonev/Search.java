package com.harmonev;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

import util.GlueList;
import util.TextFileUtils;

public class Search {
	private static String relationStartEndCleanFinal = "E:/ConceptNet/data/relation_start_end_clean_final.csv";
	private static String styleSheet =
			"graph {" +
            "	text-size: 20;" +
            "}" +
			"node {" +
            "	fill-color: black;" +
            "	text-size: 16;" +
            "}" +
            "node.root {" +
            "	fill-color: red;" +
            "}" +
            "node.intersection {" +
            "	fill-color: blue;" +
            "}" +
            "edge {" +
            "	text-size: 16;" +
            "}" +
            "edge.common {" +
            "	fill-color: purple;" +
            "}";
	
	public Graph createGraphFromFile() {
		long start = System.currentTimeMillis();
		Graph graph = new SingleGraph("RelationStartEnd", false, true);
		GlueList<String> lines = TextFileUtils.load(relationStartEndCleanFinal);
		
		for (String s : lines) {
			String[] values = s.split("\t");
			String relation = values[0];
			String startNode = values[1];
			String endNode = values[2];
			
			graph.addEdge(startNode + " " + endNode, startNode, endNode, true)
				.addAttribute("relation", relation);
		}
		
		System.out.println("Graph created in " + ((System.currentTimeMillis() - start) / 1000f) + " seconds.");
		
		return graph;
	}
	
	public Graph buildConceptSpace(List<String> concepts) {
		Graph graph = createGraphFromFile();
		GlueList<String> paths = new GlueList<>();
		HashSet<String> temp = new HashSet<>();
		
		for (String r : concepts) {
			graph.getNode(r).addAttribute("root", true);
		}
		
		// start with two nodes and build a graph
		// we then take the next node and find connections
		// to the unique nodes of the built graph and repeat
		temp.add(concepts.get(0)); // initial end node to kick things off
		for (int i = 1; i < concepts.size(); i++) {
			Node startNode = graph.getNode(concepts.get(i));
			startNode.addAttribute("layer", 0);
			startNode.addAttribute("visited", true);
			paths.addAll(traverse(startNode, temp, i, 1));
			
			temp.clear();
			temp.addAll(getUniqueConcepts(paths));
		}
		
		Graph newGraph = getGraph(graph, paths);
		newGraph.addAttribute("ui.stylesheet", styleSheet);
		for (String c : concepts) {
			newGraph.getNode(c).setAttribute("ui.class", "root");
		}
		
		newGraph.display();
		
		return null;
	}
	
	// main traversal method
	// breadth-first traversal until ALL roots are found
	// add any non roots along the way and stops when the last node is searched
	// on the layer the last root was found
	private GlueList<String> traverse(Node root, HashSet<String> endConcepts, int rootsToFind, int depth) {
		int layerFoundOn = 9999;
		int currentLayer = 0;
		int rootsFound = 0;
		StringBuilder sb = new StringBuilder();
		GlueList<String> paths = new GlueList<String>();
		GlueList<Node> nodesToClear = new GlueList<Node>();
		Queue<Node> currentQueue = new ArrayDeque<>();
		Queue<Node> nextQueue = new ArrayDeque<>();
		Queue<Node> temp;

		currentQueue.add(root);
		nodesToClear.add(root);
		
		while (!currentQueue.isEmpty()) {
			Node parentNode = currentQueue.remove();
			if (currentLayer > layerFoundOn + (depth - 1)) {
				continue;
			}
			
			Iterator<Node> iter = parentNode.getNeighborNodeIterator();
			
			while (iter.hasNext()) {
				Node n = iter.next();
				nodesToClear.add(n);
				if (n.getAttribute("visited") != null) {
					continue;
				}
				
				n.addAttribute("parent", parentNode);
				
				// when we find the end concept we want to connect to...
				if (endConcepts.contains(n.getId())) {
					Node p = parentNode;
					sb.setLength(0);
					
					// traverse backwards while storing the path
					sb.append(n.getId() + " ");
					while (p != root) {
						sb.append(p.getId() + " ");
						p = p.getAttribute("parent");
					}
					sb.append(p.getId());
					paths.add(sb.toString());
					
					// mark the layer we found the first path so we can stop traversal
					// but want to continue on with the current layer in case
					// there are more connecting paths in the current layer
					if (n.hasAttribute("root")) {
						rootsFound++;
					}
					
					if (rootsFound == rootsToFind && layerFoundOn == 9999) {
						layerFoundOn = currentLayer;
					}
				} else {
					n.addAttribute("visited", true);
					nextQueue.add(n);
				}
			}
			
			if (currentQueue.isEmpty()) {
				currentLayer++;
				temp = nextQueue;
				nextQueue = currentQueue;
				currentQueue = temp;
				nextQueue.clear();
			}
		}
		
		// need to clear all the attributes from the nodes so we can reuse the reference graph
		// without collision on other traversals
		for (Node n : nodesToClear) {
			n.removeAttribute("parent");
			n.removeAttribute("visited");
		}
		
		return paths;
	}
	
	private Set<String> getUniqueConcepts(GlueList<String> paths) {
		Set<String> result = new HashSet<>();
		
		for (String p : paths) {
			for (String c : p.split(" ")) {
				result.add(c);
			}
		}
		
		return result;
	}
	
	// takes a list of paths and create a graph that contains
	// the bi-directional relation and the nodes in the the paths
	// using the original master graph as a traversal tool and reference
	private Graph getGraph(Graph originalGraph, GlueList<String> paths) {
		Graph graph = new SingleGraph("graph", false, true);
		StringBuilder sb = new StringBuilder(); // to print out the relations
		
		for (String p : paths) {
			String[] concepts = p.split(" ");
			sb.setLength(0);
			
			for (int i = 0; i < concepts.length - 1; i++) {
				String normalEdge = concepts[i] + " " + concepts[i+1];
				String reverseEdge = concepts[i+1] + " " + concepts[i];
				boolean hasNormal = originalGraph.getEdge(normalEdge) != null;
				boolean hasReverse = originalGraph.getEdge(reverseEdge) != null;
				
				sb.append(concepts[i]);
				
				if (hasNormal) {
					String rel = originalGraph.getEdge(normalEdge).getAttribute("relation");
					sb.append(" --" + rel + "-> ");
					graph.addEdge(normalEdge, concepts[i], concepts[i+1], true)
					.addAttribute("ui.label", rel);
				}
				
				if (hasReverse) {
					String rel = originalGraph.getEdge(reverseEdge).getAttribute("relation");
					sb.append((hasNormal ? " & " : "") + " <-" + rel + "-- ");
					graph.addEdge(reverseEdge, concepts[i+1], concepts[i], true)
					.addAttribute("ui.label", originalGraph.getEdge(reverseEdge).getAttribute("relation"));
				}
				
				sb.append(i == concepts.length - 2 ? concepts[i+1] : "");
				
				graph.getNode(concepts[i]).addAttribute("ui.label", concepts[i]);
				graph.getNode(concepts[i+1]).addAttribute("ui.label", concepts[i+1]);
			}
			
			System.out.println(sb.toString());
		}
		
		return graph;
	}
}
