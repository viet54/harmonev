package com.harmonev;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

import com.google.common.base.Strings;
import com.harmonev.conceptnet.CNetVectors;
import com.harmonev.conceptnet.ConceptNetAPI;
import com.harmonev.conceptnet.Language;
import com.harmonev.conceptnet.Relations;
import com.harmonev.conceptnet.models.CNetEdge;
import com.harmonev.conceptnet.models.Lookup;
import com.harmonev.conceptnet.uri.ConceptURI;
import com.harmonev.conceptnet.uri.UriHierarchy;

public class Search2 {
	ArrayList<NTree<String>> trees = new ArrayList<NTree<String>>();
	private static CNetVectors vectors = new CNetVectors();
	private static StringBuilder resultData = new StringBuilder();
	
	private static String styleSheet =
			"graph {" +
            "	text-size: 20;" +
            "}" +
			"node {" +
            "	fill-color: black;" +
            "	text-size: 16;" +
            "}" +
            "node.root {" +
            "	fill-color: red;" +
            "}" +
            "node.intersection {" +
            "	fill-color: blue;" +
            "}" +
            "edge {" +
            "	text-size: 16;" +
            "}" +
            "edge.common {" +
            "	fill-color: purple;" +
            "}";
	public static Lookup lookUp(String concept, int limit) {
		return Strings.isNullOrEmpty(concept) ? null : ConceptNetAPI.getLookup(new ConceptURI(concept), limit);
	}
	
	public static Lookup lookUp(String concept) {
		return lookUp(concept, 50);
	}
	
	public static boolean isRelated(String firstConcept, String secondConcept) {
		return !getRelations(firstConcept, secondConcept, 1).isEmpty();
	}
	
	public static ArrayList<String> harmonEv(Graph graph, String root1, String root2, String nounType, int limit) {
		ArrayList<String> search = Search2.getRelationChild(Relations.IS_A, nounType, limit);
		ArrayList<String> result = new ArrayList<>(search.size());
		for (String s : search) {
			result.add(Search2.rank(graph, root1, root2, s) + " " +  s);
		}
		Collections.sort(result);
		return result;
	}
	
	// iterating through all the vectors and see which one is closest to the
	// roots concept space
	public static ArrayList<String> rankAll(Graph graph, String root1, String root2) {
		ArrayList<String> result = new ArrayList<>(CNetVectors.LINE_COUNT);
		for (String s : getVector().getAllVectorString()) {
			result.add(Search2.rank(graph, root1, root2, s) + " " +  s);
		}
		Collections.sort(result);
		return result;
	}
	
	// TODO once you have a graph of two (or more) concepts and want to see
	// which object concept is closest we do vector distance of the concept space
	// of the root concepts to the object concepts and get the average
	// need to weigh the concept space concept somehow and add that into the equation of
	// averaging, maybe multiple the vector distance of each concept in the space
	// including the root concepts to the object concept
	public static void rank(String rootConcept1, String rootConcept2, String objectConcept) {
		Graph graph = evolve(rootConcept1, rootConcept2, 10, 1000, 2);
		rank(graph, rootConcept1, rootConcept2, objectConcept);
	}
	
	public static double rank(Graph graph, String rootConcept1, String rootConcept2, String objectConcept) {
		if (graph.getNode(objectConcept) != null) {
			System.out.println("Concept already contained in concept space!");
			return 0;
		}
		
		double totalWeight = 0;
		for (Node n : graph.getNodeSet()) {
			if (n.getAttribute("vectorWeight") != null) {
				totalWeight += (double) n.getAttribute("vectorWeight");
			}
		}
		
		double sum = 0;
		int count = 0;
		boolean normalize = true;
		ArrayList<String> result = new ArrayList<String>(graph.getNodeSet().size());
		for (Node n : graph.getNodeSet()) {
			float[] objectVector = getVector().getVectorFloats(objectConcept);
			if (objectVector == null) {
				continue;
			}
			
			String nodeConcept = n.getId();
			if (nodeConcept.equals(rootConcept1) || nodeConcept.equals(rootConcept2)) {
				double distance = 0f;
				float[] vector1 = getVector().getVectorFloats(nodeConcept);

				if (normalize) {
					double rootToObject = CNetVectors.getDistance(vector1, objectVector);
					distance = rootToObject * ((double) n.getAttribute("vectorWeight") / totalWeight); // weigh by vector weights
				} else {
					distance = CNetVectors.getDistance(vector1, objectVector); // weigh by addition
				}
				
				sum += distance;
				count++;
				result.add(distance + " " + objectConcept + " " + nodeConcept);
			} else {
				double distance = 0f;
				float[] child = getVector().getVectorFloats(nodeConcept);
				float[] root1 = getVector().getVectorFloats(rootConcept1);
				float[] root2 = getVector().getVectorFloats(rootConcept2);
				double childToObject = CNetVectors.getDistance(child, objectVector);
				
				if (normalize) {
					distance = childToObject * ((double) n.getAttribute("vectorWeight") / totalWeight); // weigh by vector weights
				} else {
					double childToRoot1 = CNetVectors.getDistance(child, root1);
					double childToRoot2 = CNetVectors.getDistance(child, root2);
					distance = childToRoot1 + childToRoot2 + childToObject; // weigh by addition
				}
				
				sum += distance;
				count++;
				result.add(distance + " " + objectConcept + " " + nodeConcept);
			}
		}
		
		Collections.sort(result.subList(0, result.size()));
//		for (String s : result) {
//			System.out.println(s);
//		}
		
//		System.out.println("average for " + objectConcept + ": " + (sum / count));
		
		return sum / count;
	}
	
	public static Graph evolve(String startConcept, String endConcept, int topConceptCount, int searchLimit, int layerLimit) {
		resultData.setLength(0);
		Graph graph = new SingleGraph("concept");
		graph.addAttribute("ui.stylesheet", styleSheet);
		
		float[] vector1 = getVector().getVectorFloats(startConcept);
		float[] vector2 = getVector().getVectorFloats(endConcept);
		double distanceBetweenRoots = CNetVectors.getDistance(vector1, vector2);
		
		Node startNode = graph.addNode(startConcept);
		startNode.addAttribute("ui.label", startConcept);
		startNode.addAttribute("vectorWeight", distanceBetweenRoots);
		startNode.setAttribute("ui.class", "root");
		
		Node endNode = graph.addNode(endConcept);
		endNode.addAttribute("ui.label", endConcept);
		endNode.addAttribute("vectorWeight", distanceBetweenRoots);
		endNode.setAttribute("ui.class", "root");
		
		connectConcepts(graph, startConcept, endConcept, topConceptCount, searchLimit, layerLimit, 0);
		removeGraphDeadEnds(graph);
		printDisplayData(graph);
		graph.display();
		
		return graph;
	}
	
	public static ArrayList<String> getRelationChild(Relations relation, String childConcept, int limit) {
		ArrayList<String> childConcepts = new ArrayList<>(limit);
		Lookup lookup = ConceptNetAPI.getSearch(Relations.IS_A, "end", new ConceptURI(childConcept).getURI(), limit);
		for (CNetEdge edge : lookup.getEdges()) {
			UriHierarchy uri = edge.getStart();
			if (uri.getLang() == Language.EN && !childConcepts.contains(uri.getConceptText())) {
				childConcepts.add(uri.getConceptText());
			}
		}
		
		return childConcepts;
	}
	
	private static void removeGraphDeadEnds(Graph graph) {
		boolean removeDeadEnd = true;
		while (removeDeadEnd) {
			boolean deadEndFound = false;
			for (Node node : graph.getNodeSet()) {
				if (node.getEdgeSet().size() == 1) {
					graph.removeNode(node);
					deadEndFound = true;
				}
			}
			
			if (!deadEndFound) {
				break;
			}
		}
	}
	
	private static void printDisplayData(Graph graph) {
		String[] lines = resultData.toString().split(System.getProperty("line.separator"));
		for (int i = 0; i < lines.length; i++) {
			if (graph.getNode(lines[i].split(" ")[1]) == null) {
				lines[i] = "";
			}
		}
		Arrays.sort(lines);
		
		StringBuilder finalStringBuilder = new StringBuilder();
		for (String s : lines) {
			if (!s.equals("")) {
				finalStringBuilder.append(s).append(System.getProperty("line.separator"));
			}
		}
		System.out.println(finalStringBuilder.toString());
	}
	
	public static void connectConcepts(Graph graph, String startConcept, String endConcept, int topConceptCount, int searchLimit, int layerLimit, int currentLayer) {
		if (currentLayer >= layerLimit) {
			return;
		}
		ArrayList<LookupResult> startConceptSpace = simpleLookup(startConcept, searchLimit);
		List<LookupResult> result = getTopClosestConcepts(startConceptSpace, endConcept, topConceptCount);
		
		for (LookupResult r : result) {
			String end = r.getChildConcept();
			String edgeLabel = r.getEdgeId();
			
			// connect child nodes to parent
			if (graph.getNode(end) == null) {
				graph.addNode(end).addAttribute("ui.label", end);
				Edge edge = graph.addEdge(edgeLabel, r.getFromConcept(), r.getToConcept(), true);
				edge.addAttribute("ui.label", r.getRelType());
				resultData.append(r.getDistanceToParent() + " " + r.getChildConcept() + " " +
						(layerLimit - currentLayer) + System.getProperty("line.separator"));
				// store the vector distance weight
				if (graph.getNode(end).getAttribute("vectorWeight") == null) {
					float[] vector1 = getVector().getVectorFloats(end);
					float[] vector2 = getVector().getVectorFloats(endConcept);
					double distanceToEndConcept = CNetVectors.getDistance(vector1, vector2);
					double totalWeight = r.getDistanceToParent() + distanceToEndConcept;
					graph.getNode(end).addAttribute("vectorWeight", totalWeight);
				}
				
				// connect to end concept if relation exists
				// else keep traversing
				ArrayList<LookupResult> relations = getRelations(end, endConcept, 1);
				if (relations.size() > 0) {
					// just picking the first relation that returns
					edge = graph.addEdge(relations.get(0).getEdgeId(), relations.get(0).getFromConcept(), relations.get(0).getToConcept(), true);
					edge.addAttribute("ui.label", relations.get(0).getRelType());
				} else {
					connectConcepts(graph, end, endConcept, topConceptCount, searchLimit, layerLimit, currentLayer + 1);
				}
			}
		}
	}
	
	public static ArrayList<LookupResult> getRelations(String firstConcept, String secondConcept, int limit) {
		ArrayList<LookupResult> relationSearchResult = new ArrayList<>(limit * 2); // times two for normal + reverse results
		
		Lookup lookupNormal = searchRelations(firstConcept, secondConcept, limit);
		if (lookupNormal != null) {
			for (CNetEdge edge : lookupNormal.getEdges()) {
				if (edge.getRel() != null) {
					relationSearchResult.add(new LookupResult(firstConcept, secondConcept, true, edge.getWeight(), edge.getRel()));
				}
			}
		}
		
		Lookup lookupReversed = searchRelations(secondConcept, firstConcept, limit);
		if (lookupReversed != null) {
			for (CNetEdge edge : lookupReversed.getEdges()) {
				if (edge.getRel() != null) {
					relationSearchResult.add(new LookupResult(secondConcept, firstConcept, false, edge.getWeight(), edge.getRel()));
				}
			}
		}
		
		return relationSearchResult;
	}
	
	private static Lookup searchRelations(String startConcept, String endConcept, int limit) {
		return Strings.isNullOrEmpty(startConcept) || Strings.isNullOrEmpty(endConcept) ?
				null : ConceptNetAPI.getSearch(startConcept, endConcept, limit);
	}
	
	// find the distance between all concept space concepts to the end concept and sort by shortest distance
	public static ArrayList<LookupResult> getSortedConceptSpace(ArrayList<LookupResult> startConceptSpace, String endConcept) {
		StringBuilder sb = new StringBuilder();
		ArrayList<String> results = new ArrayList<String>(startConceptSpace.size());
		
		for (int i = 0; i < startConceptSpace.size(); i++) {
			sb.setLength(0);
			float[] vector1 = getVector().getVectorFloats(startConceptSpace.get(i).getChildConcept());
			float[] vector2 = getVector().getVectorFloats(endConcept);
			if (vector1 != null && vector2 != null) {
				double distance = CNetVectors.getDistance(vector1, vector2);
				sb.append(distance);
				sb.append(" " + i);
				results.add(sb.toString());
			}
		}
		
		Collections.sort(results.subList(0, results.size()));
		ArrayList<LookupResult> sortedConceptSpace = new ArrayList<LookupResult>(results.size());
		for (int i = 0; i < results.size(); i++) {
			String[] sortedResult = results.get(i).split(" ");
			float distance = Float.parseFloat(sortedResult[0]);
			int index = Integer.parseInt(sortedResult[1]);
			startConceptSpace.get(index).setDistanceToParent(distance);
			sortedConceptSpace.add(startConceptSpace.get(index));
		}
		
		return sortedConceptSpace;
	}
	
	public static List<LookupResult> getTopClosestConcepts(ArrayList<LookupResult> startConceptSpace, String endConcept, int topConceptCount) {
		ArrayList<LookupResult> sortedConceptSpace = getSortedConceptSpace(startConceptSpace, endConcept);
		
		// override on pruning down to just the topConceptCount but to keep every
		// concept that's under a threshold that could potentially reach the endConcept in one jump
		// which is currently set at the observed value of 1.2f
		int returnCount = 0;
		for (LookupResult r : sortedConceptSpace) {
			if (r.getDistanceToParent() <= 1.2f) {
				returnCount++;
			} else {
				break;
			}
		}
		
		returnCount = returnCount > topConceptCount ? returnCount : topConceptCount; // return minimum of topConceptCount
		returnCount = returnCount < sortedConceptSpace.size() ? returnCount : sortedConceptSpace.size();
		
		return sortedConceptSpace.subList(0, returnCount);
	}
	
	public static ArrayList<LookupResult> simpleLookup(String parentConcept, int limit) {
		ArrayList<LookupResult> results = new ArrayList<LookupResult>(limit);
		Lookup lookup = lookUp(parentConcept, limit);
		if (lookup != null) {
			for (CNetEdge edge : lookup.getEdges()) {
				boolean relNormal = parentConcept.equals(edge.getStart().getConceptText());
				UriHierarchy uri = relNormal ? edge.getEnd() : edge.getStart();
				String childConcept = uri.getConceptText();
				if (childConcept == null) {
					continue;
				}
				
				LookupResult result = new LookupResult(parentConcept, childConcept, relNormal, edge.getWeight(), edge.getRel());
				if (edge.getStart().getLang() == Language.EN && !childConcept.contains("plural") && !contains(results, childConcept)) {
					results.add(result);
				}
			}
		}
		return results;
	}
	
	private static boolean contains(ArrayList<LookupResult> results, String childConcept) {
		for (LookupResult result : results) {
			if (result.getChildConcept().equals(childConcept)) {
				return true;
			}
		}
		return false;
	}
	
	public static class LookupResult {
		private String parentConcept;
		private String childConcept;
		private String edgeId;
		private boolean relationNormal;
		private float weight;
		private String relType;
		private float distanceToParent;
		
		public LookupResult(String parentConcept, String childConcept, boolean relNormal, Float weight, String relType) {
			this.parentConcept = parentConcept;
			this.childConcept = childConcept;
			this.edgeId = relNormal ? parentConcept + " " + childConcept : childConcept + " " + parentConcept;
			this.relationNormal = relNormal;
			this.weight = weight;
			this.relType = relType;
		}
		
		public String getChildConcept() {
			return childConcept;
		}
		
		public String getEdgeId() {
			return edgeId;
		}
		
		public String getFromConcept() {
			return relationNormal ? parentConcept : childConcept;
		}
		
		public String getToConcept() {
			return relationNormal ? childConcept : parentConcept;
		}
		
		public boolean getRelNormal() {
			return relationNormal;
		}
		
		public Float getWeightValue() {
			return weight;
		}
		
		public String getWeightLabels() {
			return relType;
		}

		public float getDistanceToParent() {
			return distanceToParent;
		}

		public String getRelType() {
			return relType;
		}

		public void setDistanceToParent(float distanceToParent) {
			this.distanceToParent = distanceToParent;
		}
	}
	
	private static CNetVectors getVector() {
		if (!vectors.isLoaded()) {
			vectors.load();
		}
		
		return vectors;
	}
	
	public class Config {
		private String firstConcept;
		private String secondConcept;
		private int layerCount;
		private int searchLimit;
		private float startPrunePercent;
		private float decay;
		
		public Config Config(String firstConcept, String secondConcept,
				float startPrunePercent, int layerCount, int searchLimit) {
			this.firstConcept = firstConcept;
			this.secondConcept = secondConcept;
			this.startPrunePercent = startPrunePercent;
			this.layerCount = layerCount;
			this.searchLimit = searchLimit;
			return this;
		}
		
		public Config setDecay(float decay) {
			this.decay = decay;
			return this;
		}
	}
}
