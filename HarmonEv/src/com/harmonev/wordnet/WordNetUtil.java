package com.harmonev.wordnet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import util.Stopwords;

import com.google.common.collect.Sets;

import edu.mit.jwi.IRAMDictionary;
import edu.mit.jwi.RAMDictionary;
import edu.mit.jwi.morph.WordnetStemmer;

public class WordNetUtil {
	@SuppressWarnings("unused")
	private static final String DICT_30_PATH = "dict";
	private static final String DICT_31_PATH = "dict3.1";
	private static final String DICT_PATH = DICT_31_PATH;
	
	private static IRAMDictionary dict;
	private static WordnetStemmer stemmer;
	
	static {
		try {
			dict = new RAMDictionary(new URL("file", null, DICT_PATH));
			dict.open();
			dict.load(true);
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		} catch (InterruptedException e) {
		}
		
		stemmer = new WordnetStemmer(getDict());
	}
	

	
	public static IRAMDictionary getDict() {
		return dict;
	}
	
	public static WordnetStemmer getStemmer() {
		return stemmer;
	}
	
	public static Set<String> getFilteredSet(List<String> words) {
		return Sets.difference(new HashSet<String>(words), Stopwords.STOPWORDS_DEFAULT);
	}
}
