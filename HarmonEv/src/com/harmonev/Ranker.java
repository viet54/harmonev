package com.harmonev;

import java.util.ArrayList;
import java.util.Collections;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import util.Matrix;

public class Ranker {
	private LegacySearch search;
	private String[] concepts;
	
	public Ranker(LegacySearch search) {
		this.search = search;
	}
	
	public void rank() {
		float[][] m = parseGraph();
		Matrix matrix = new Matrix(m);
		float[] result = matrix.getMarkovChain(5);
		
		// sorting
		ArrayList<String> sorted = new ArrayList<String>();
		for (int i = 0; i < concepts.length; i++) {
			sorted.add(result[i] + " " + concepts[i]);
		}
		
		Collections.sort(sorted);
		Collections.reverse(sorted);
		for (String s : sorted) {
			System.out.println(s);
		}
	}
	
	private float[][] parseGraph() {
		Graph graph = search.getGraph();
		Node[] nodes = graph.getNodeSet().toArray(new Node[graph.getNodeSet().size()]);
		concepts = new String[nodes.length];
		String[][] edgeMatrix = new String[concepts.length][concepts.length];
		float[][] edgeWeight = new float[concepts.length][concepts.length];
		
		for (int i = 0; i < nodes.length; i++) {
			concepts[i] = nodes[i].getAttribute("ui.label");
		}
		
		// TODO BUG, row and column are swapped?
		// not correct
		
		// build the weight matrix
		for (int i = 0; i < concepts.length; i++) {
			for (int j = 0; j < concepts.length; j++) {
				edgeMatrix[j][i] = concepts[i] + " " + concepts[j];
				System.out.print(edgeMatrix[j][i] + " ");
				
				Edge edge = graph.getEdge(concepts[i] + " " + concepts[j]) == null ? graph.getEdge(concepts[j] + " " + concepts[i]) == null ? null : 
					graph.getEdge(concepts[j] + " " + concepts[i]) : graph.getEdge(concepts[i] + " " + concepts[j]);
				edgeWeight[j][i] = edge == null ? 0f : (float) edge.getAttribute("edgeWeight");
				
				if (edge == null) {
					System.out.println("No edge found: " + edgeMatrix[j][i] + " " + edgeWeight[j][i]);
				} else {
					System.out.println("Edge found: " + edgeMatrix[j][i] + " " + edgeWeight[j][i]);
				}
			}
			System.out.println();
		}
		
		// normalize the weights
		for (int i = 0; i < concepts.length; i++) {
			float columnTotal = 0f;
			
			for (int j = 0; j < concepts.length; j++) {
				columnTotal += edgeWeight[j][i];
			}
			
			for (int j = 0; j < concepts.length; j++) {
				edgeWeight[j][i] = edgeWeight[j][i] / columnTotal;
			}
		}
		
		
		return edgeWeight;
	}
}
