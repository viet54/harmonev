package com.harmonev;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.graphstream.graph.Graph;

import com.harmonev.conceptnet.Assertions;
import com.harmonev.conceptnet.CNetVectors;
import com.harmonev.conceptnet.Relations;

@SuppressWarnings("serial")
public class Main extends JPanel implements ActionListener {
	protected JTextField textField;
	protected JTextArea textArea;
	private final static String newline = "\n";
	Graph graph = null;
//	private static String DB_PATH = "jdbc:sqlite:d:/ConceptNet/VectorMatrix.db";
	private static String DB_PATH = "jdbc:sqlite:d:/ConceptNet/assertions.db.2";

	public static void main(String[] args) throws FileNotFoundException {
		// Search search = new Search("future harness");
		// try {
		// // search.searchLookup(1, 5, true);
		// search.searchHybrid(1, 50);
		// search.printTrees();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		//
		// for (String word : search.getIntersection()) {
		// System.out.println(word);
		// }
		//
		// search.displayGraph();
		// //
		// // Ranker ranker = new Ranker(search);
		// // ranker.rank();

//		javax.swing.SwingUtilities.invokeLater(new Runnable() {
//			public void run() {
//				createAndShowGUI();
//			}
//		});
		
//		for (String s : Search.getRelations("future", "harness", 10).getResult()) {
//			System.out.println(s);
//		}
		
//		Search2.rank("addiction", "love", "mirror");
		
//		createNewDatabase("assertions.db.2");
//		selectStar();
//		insert();
//		delete();
//		select();
		
//    	CNetVectors vectors = new CNetVectors();
//    	vectors.load();
//    	vectors.serializeVectors();
//    	vectors.serializeHashMap();
//    	for (int i = 0; i < 250; i++) {
//    		String vector1 = vectors.getVector(i);
//    		System.out.println(vector1);
//    	}
		
//		VectorSearch vs = new VectorSearch();
//		vs.search("metal", "addiction");
		
//    	CNetVectors vectors = new CNetVectors(12);
//    	vectors.load();
//    	ArrayList<String> concepts = new ArrayList<>();
//    	concepts.add("color");
//    	concepts.add("love");
//    	ArrayList<Data> data = vectors.getClosestConcepts(concepts);
//    	int i = 0;
//    	for (Data d : data) {
//    		if (Search2.isRelated(concepts.get(0), d.concept) &&
//    				Search2.isRelated(concepts.get(1), d.concept)) {
//    			System.out.println(d.concept + " " + d.sum);
//    			i++;
//    		}
    		
//    		System.out.println(d.concept);
//    		
//    		i++;
//    		
//    		if (i == 100) {
//    			break;
//    		}
//    	}
		
//		for (LookupResult l : Search2.simpleLookup("color", 50)) {
//			System.out.println(l.getChildConcept());
//		}
		
		Search s = new Search();
		ArrayList<String> concepts = new ArrayList<>();
		concepts.add("addiction");
		concepts.add("memory");
		concepts.add("love");
		concepts.add("orange");
		s.buildConceptSpace(concepts);
//		a.pageRank("addiction", "memory", 1);
	}

	public Main() {
		super(new GridBagLayout());

		textField = new JTextField(100);
		textField.addActionListener(this);

		textArea = new JTextArea(50, 100);
		textArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textArea);

		// Add Components to this panel.
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = GridBagConstraints.REMAINDER;

		c.fill = GridBagConstraints.HORIZONTAL;
		add(textField, c);

		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.weighty = 1.0;
		add(scrollPane, c);
	}

	public void actionPerformed(ActionEvent evt) {
		String[] tokens = textField.getText().split(" ");
		
		switch (tokens[0]) {
		case "l":
			switch (tokens.length) {
			case 2:
				textArea.append(LegacySearch.lookUp(tokens[1]).getJson() + newline);
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				List<String> result = LegacySearch.getTopClosestConcepts(
						tokens[1], tokens[2], Integer.parseInt(tokens[3]), Integer.parseInt(tokens[4]));
				for (String s : result) {
					textArea.append(s + newline);;
				}
				break;
			}
			break;
		case "g":
			switch (tokens.length) {
			case 2:
				LegacySearch.graph(tokens[1]);
				break;
			case 3:
				LegacySearch.graph(tokens[1], Integer.parseInt(tokens[2]));
				break;
			case 4:
				LegacySearch.graph(tokens[1], Integer.parseInt(tokens[2]), "y".equals(tokens[3]), false);
				break;
			case 5:
				LegacySearch.graph(tokens[1], Integer.parseInt(tokens[2]), "y".equals(tokens[3]), "y".equals(tokens[4]));
				break;
			}
			break;
		case "a":
			switch (tokens.length) {
			case 2:
				textArea.append(LegacySearch.association(tokens[1]) + newline);
				break;
			case 3:
//				textArea.append(Search.associationBetweenTwoConcepts(tokens[1], tokens[2]) + newline);
				textArea.append(LegacySearch.associationTopConcepts(tokens[1], tokens[2]) + newline);
				break;
			}
			break;
		case "r":
			switch (tokens.length) {
			case 3:
				graph = Search2.evolve(tokens[1], tokens[2], 5, 1000, 3);
				break;
			case 4:
				if ("all".equals(tokens[3])) {
					ArrayList<String> search = Search2.rankAll(graph, tokens[1], tokens[2]);
					for (int i = 0; i < 50; i++) {
						System.out.println(search.get(i));
					}
				} else {
					ArrayList<String> search = Search2.harmonEv(graph, tokens[1], tokens[2], tokens[3], 1000);
					for (String s : search) {
						System.out.println(s);
					}
				}
				break;
			}
			break;
		case "e":
			switch (tokens.length) {
			case 3:
				textArea.append(LegacySearch.isRelated(tokens[1], tokens[2]) + newline);
				break;
			case 4:
				for (String s : LegacySearch.getRelations(tokens[1], tokens[2], Integer.parseInt(tokens[3])).getResult()) {
					textArea.append(s + newline);
				}
				break;
			}
			break;
		case "s":
			switch (tokens.length) {
			case 3:
				ArrayList<String> search = Search2.getRelationChild(Relations.IS_A, tokens[1], Integer.parseInt(tokens[2]));
				for (String s : search) {
					System.out.println(s);
				}
				break;
			case 4:
				Search2.rank(graph, tokens[1], tokens[2], tokens[3]);
				break;
			}
			break;
		case "clear":
			textArea.setText(null);
			break;
		}
		
		textField.selectAll();

		// Make sure the new text is visible, even if there
		// was a selection in the text area.
		textArea.setCaretPosition(textArea.getDocument().getLength());
	}

	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event dispatch thread.
	 */
	private static void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("TextDemo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Add contents to the window.
		frame.add(new Main());

		// Display the window.
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
    public static void createNewDatabase(String fileName) {
    	 
        String url = "jdbc:sqlite:d:/ConceptNet/" + fileName;
 
        try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
//                DatabaseMetaData meta = conn.getMetaData();
//                System.out.println("The driver name is " + meta.getDriverName());
//                System.out.println("A new database has been created.");
                
                System.out.println("Opened database successfully");

                Statement stmt = conn.createStatement();
                String sql = "CREATE TABLE VECTOR_DISTANCES " +
                             "(VECTOR1           TEXT    NOT NULL, " + 
                             " VECTOR2           TEXT     NOT NULL, " + 
                             " DISTANCE        REAL)"; 
                stmt.executeUpdate(sql);
                stmt.close();
                conn.close();
            }
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
	public static void insert() {
		// TODO last STOPPED before index 30
		CNetVectors vectors = new CNetVectors();
		vectors.load();

		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(DB_PATH);
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

			for (int i = 10; i < 30; i++) {
				long start = System.currentTimeMillis();
				String vector1 = vectors.getVector(i);
				
				for (String s : vectors.getAllVectorString()) {
					if (s.equals(vector1)) {
						continue;
					}
					
					double val = CNetVectors.getDistance(vectors.getVectorFloats(vector1), vectors.getVectorFloats(s));
					stmt = c.createStatement();
					String sql = String.format("INSERT INTO VECTOR_DISTANCES (VECTOR1,VECTOR2,DISTANCE) " + "VALUES (\"%s\", \"%s\", %s);", vector1, s, val);
					stmt.executeUpdate(sql);
				}
				c.commit(); // autocommit on
				stmt.clearBatch();
				
				System.out.println("Last commit index: " + i + " for entry \"" + vector1 + "\"");
				System.out.println("Seconds taken: " + ((System.currentTimeMillis() - start) / 1000));
			}

			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Records created successfully");
	}
    
    public static void select() {
    	Connection c = null;
        Statement stmt = null;
        try {
          Class.forName("org.sqlite.JDBC");
          c = DriverManager.getConnection(DB_PATH);
          c.setAutoCommit(false);
          System.out.println("Opened database successfully");

          stmt = c.createStatement();
          ResultSet rs = stmt.executeQuery( "SELECT * FROM VECTOR_DISTANCES;" );
          while ( rs.next() ) {
             String  vector1 = rs.getString("VECTOR1");
             String  vector2 = rs.getString("VECTOR2");
             double value = rs.getDouble("DISTANCE");
             System.out.println( "VECTOR1 = " + vector1 );
             System.out.println( "VECTOR2 = " + vector2 );
             System.out.println( "DISTANCE = " + value );
             System.out.println();
          }
          rs.close();
          stmt.close();
          c.close();
        } catch ( Exception e ) {
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
          System.exit(0);
        }
        System.out.println("Operation done successfully");
    }
    
    public static void selectStar() {
    	Connection c = null;
        Statement stmt = null;
        try {
          Class.forName("org.sqlite.JDBC");
          c = DriverManager.getConnection(DB_PATH);
          c.setAutoCommit(false);
          System.out.println("Opened database successfully");

          stmt = c.createStatement();
          ResultSet rs = stmt.executeQuery( "SELECT name FROM sqlite_master WHERE type='table';" );
//          ResultSet rs = stmt.executeQuery( "SELECT * FROM text_index LIMIT 1;" ); // text_index // VECTOR_DISTANCES
//          ResultSet rs = stmt.executeQuery( "PRAGMA table_info(VECTOR_DISTANCES)" ); // text_index // VECTOR_DISTANCES
          while ( rs.next() ) {
             System.out.println(rs.getString(1));
          }
          rs.close();
          stmt.close();
          c.close();
        } catch ( Exception e ) {
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
          System.exit(0);
        }
        System.out.println("Operation done successfully");
    }
    
    public static void delete() {
    	Connection c = null;
        Statement stmt = null;
        try {
          Class.forName("org.sqlite.JDBC");
          c = DriverManager.getConnection(DB_PATH);
          c.setAutoCommit(false);
          System.out.println("Opened database successfully");

          stmt = c.createStatement();
          String sql = "DROP TABLE VECTOR_DISTANCES;"; 
          stmt.execute(sql);

          stmt.close();
          c.commit();
          c.close();
        } catch ( Exception e ) {
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
          System.exit(0);
        }
        System.out.println("Records created successfully");
    }
}
